/*!
 * Accessing dps.report's API. This allows for uploading logs, downloading Log Json data.
 * Requires the `reqwest` feature to work send api requests. Can be used without for type binding e.g. in a wasm enviroment.
 * # Usage
 * ```
 * // Create a api instance with the default server url:
 * let api = DpsReportApi::default();
 * // upload a log file
 * api.upload_file("C:/Users/pv42/documents/Guild Wars 2/addons/arcdps/20240108031415.evtc");
 * // Download JSON Data:
 * api.get_json_data_by_link("https://dps.report/zUUd-20240310-224140_cerus");
 * ```
 * When downloading JSON data consider using `get_ei_data_cached()` instead for local caching.
 * ```
* # use elite_insight_json::dps_report_api::get_ei_data_cached;
* get_ei_data_cached("https://dps.report/zUUd-20240310-224140_cerus", "D:/GW2LogCache/");
 * ```
 */
use std::collections::HashMap;
use std::fs;
use std::fs::File;
use std::io::{BufReader, Write};
use std::path::Path;

use flate2::read::GzDecoder;
use flate2::write::GzEncoder;
use flate2::Compression;
use regex::Regex;

#[cfg(feature = "reqwest")]
use reqwest;
#[cfg(feature = "reqwest")]
use reqwest::blocking;
#[cfg(feature = "reqwest")]
use reqwest::blocking::multipart;
#[cfg(feature = "reqwest")]
use reqwest::blocking::multipart::Part;
#[cfg(feature = "reqwest")]
use reqwest::blocking::Client;
#[cfg(feature = "reqwest")]
use reqwest::Url;

use serde::{Deserialize, Serialize};

#[cfg(feature = "reqwest")]
use crate::ei_dto::ei_data_types::EliteInsightData;

#[cfg(feature = "reqwest")]
const DEFAULT_CACHE_PATH: &str = "E:/GW2LogsData/logJsons/";

/// An error returned by the dps.report api
#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct DpsReportApiError {
    /// Error message
    pub error: String,
}

// TODO Result<T,DpsReportApiError> ?
/// Result of any api access to the DPS report api. Can be either successful or an error.  
#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(untagged)]
pub enum DpsReportResult<T> {
    /// Successfully returned Data
    Data(T),
    /// An error indicated by the dps.report api
    Error(DpsReportApiError),
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct DpsReportApiResultEncounter {
    /// CURRENTLY NON-FUNCTIONAL. Encounter 'unique identifier', computed from instance ids. This attempts to uniquely identify raid attempts/encounters across users. This -WILL- be set to `None` if an identifier cannot be uniquely generated!
    pub unique_id: Option<String>,
    /// If encounter was successful (boss kill) or not.
    pub success: bool,
    // todo time
    /// Time in seconds of encounter. This may not be accurate with server delays and evtc logging issues.
    pub duration: Option<f32>,
    /// Computed overall DPS of the group.
    pub comp_dps: Option<i64>,
    /// Number of players in the encounter
    pub number_of_players: i32,
    // TODO null when
    /// Number of party/squad groups in the encounter
    pub number_of_groups: Option<i32>,
    /// Boss ID of encounter. See https://dps.report/docs/bossIds.txt
    pub boss_id: i32,
    /// Boss name.
    pub boss: String,
    /// Is encounter Challenge Mode enabled.
    #[serde(default)]
    pub is_cm: Option<bool>,
    // TODO null when
    /// GW2 client build
    #[serde(rename = "gw2Build")]
    pub gw2build: Option<u64>,
    // TODO null when
    /// Is extra encounter data available at the /getJson endpoint, this indicates that `get_json_data_by_link()` should return a valid log JSON.
    pub json_available: Option<bool>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct DpsReportApiResultEvtc {
    /// Should always return 'evtc'
    #[serde(rename = "type")]
    pub evtc_type: String,
    /// Version of ARCDPS evtc file was generated in
    pub version: String,
    /// Boss ID of encounter. Same as encounter->bossId - See <https://dps.report/docs/bossIds.txt>
    pub boss_id: i32,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
// Note this doesn't need a renaming
pub struct DpsReportApiResultPlayer {
    /// Login name of the player.
    pub display_name: String,
    /// Character name.
    pub character_name: String,
    /// Profession ID. See <https://api.guildwars2.com/v2/professions?ids=all>
    pub profession: i32,
    /// Elite spec traitline ID. See https://api.guildwars2.com/v2/specializations?ids=all
    pub elite_spec: i32,
}

/// Result of a log upload, check <https://dps.report/api> for more details
#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct DpsReportApiResult {
    /// Internal dps.report ID
    pub id: String,
    /// Generated report URL
    pub permalink: String,
    /// Future Update
    pub identifier: Option<String>,
    // todo parse unix ts
    /// Upload date, unix time format
    pub upload_time: i64,
    // todo parse unix ts
    /// Encounter date from uploaded evtc log, unix time format
    pub encounter_time: i64,
    /// Encounter date from evtc, unix time format
    pub generator: String,
    /// Internal report generator id.
    pub generator_id: i64,
    /// Internal. Report generator version.
    pub generator_version: i64,
    /// Language id from evtc. Can be en, fr, de, es, or zh.
    pub language: Option<String>,
    /// Internal language ID, numeric.
    pub language_id: Option<u8>,
    /// evtc metadata object
    pub evtc: DpsReportApiResultEvtc,
    /// Player objects, map with player names as keys. This may be empty on older arc versions.
    pub players: HashMap<String, DpsReportApiResultPlayer>,
    /// JSON Object, encounter statistics
    pub encounter: DpsReportApiResultEncounter,
    /// Generator options used when creating the report
    pub report: Option<DPSReportUploadOptions>,
    // todo: Your userToken. See userToken documentation above.
    /// Your userToken.
    pub user_token: Option<String>,
    /// Error messages upon uploading, set to `None` if none are encountered. Report may still be generated even with this set.
    pub error: Option<String>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct DPSReportUploadOptions {
    /// If report was generated with anonimized fake players.
    anonymous: bool,
    /// If report was generated with detailed players, this only applies to WvW logs.
    detailed: bool,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct DPSReportUploads {
    /// Number of pages total, use with page paramter.
    /// This might be 0 if requesting a page other 1.
    pub pages: i32,
    /// Server local time as a unix timestamp.
    pub current_time: i64,
    /// Number of uploads that match your parameters.
    /// The server might return `false` for pages other then 1, this causes deserialization to fail :(
    // TODO fix this
    pub found_uploads: u32,
    /// Total uploads of the set userToken.
    pub total_uploads: u32,
    // todo as described in User Token docs above.
    /// User Token
    pub user_token: String,
    ///  Upload Objects Array
    pub uploads: Vec<DpsReportApiResult>,
}

/// dps.report API instance
///
/// Holds a dps.report base url and allows access to the upload and JSON download function
///
/// Can be created either usng the default <https://dps.report> url with `DpsReportApi::default()` or with a alternate url with
/// `DpsReportApi::new("https://b.dps.report")`.
#[cfg(feature = "reqwest")]
pub struct DpsReportApi {
    dps_report_url: String,
}

#[cfg(feature = "reqwest")]
impl Default for DpsReportApi {
    /// Createds a dps report api instance with the default dps.report base url.
    fn default() -> Self {
        Self {
            dps_report_url: "https://dps.report".to_string(),
        }
    }
}

#[cfg(feature = "reqwest")]
impl DpsReportApi {
    /// Creates a dps report api instance with a custom base url.
    ///
    /// See <https://dps.report/api> for these available domains:  
    /// <https://dps.report/> - Cloudflare. Supports HTTPS. We've found this to be Generally Unreliable in Eastern European and Asian countries.  
    /// <http://a.dps.report/> - Imperva. Supports HTTP ONLY. Fairly reliable, use as last resort.  
    /// <https://b.dps.report/> - Sucuri. Supports HTTP and HTTPS. Preferred alternate service domain that supports HTTPS.  
    pub fn new(dps_report_url: String) -> Self {
        Self { dps_report_url }
    }

    /// Downloads a logs JSON data for a given logs permanent link.
    ///
    /// Returns `None` if the dps report instances url could not be parsed, the HTTP request failed or the response could not be parsed.
    /// In this case the error will be printed to stderr.  
    /// Returns a [DpsReportResult] otherwise which either has a [DpsReportApiError] if for example the log archive server is down or the persed JSON [EliteInsightData].
    #[cfg(feature = "reqwest")]
    pub fn get_json_data_by_link(&self, link: &str) -> Option<DpsReportResult<EliteInsightData>> {
        let url_path = "/getJson";
        let params = &[("permalink", link)];
        let mut spath = self.dps_report_url.clone();
        spath.push_str(url_path);
        match Url::parse_with_params(&spath, params) {
            Ok(url) => {
                println!("Downloading {}", url);
                match blocking::get(url) {
                    Ok(resp) => match serde_json::from_str(&resp.text().unwrap()) {
                        Ok(ei_data) => Some(ei_data),
                        Err(err) => {
                            eprintln!("Parser error for {}: {}", &link, err);
                            None
                        }
                    },
                    Err(err) => {
                        eprintln!("Error while downloading: {}", err);
                        None
                    }
                }
            }
            Err(err) => {
                eprintln!("Error parsing URL: {}", err);
                None
            }
        }
    }

    #[cfg(feature = "reqwest")]
    pub fn upload_file(&self, file_path: &String) -> Option<DpsReportResult<DpsReportApiResult>> {
        // TODO ???
        let client = Client::new();
        let mut url_str = self.dps_report_url.clone();
        url_str.push_str("/uploadContent?json=1&generator=ei");

        let file = File::open(file_path).unwrap();

        let reader = BufReader::new(file);

        //make form part of file
        let some_file = Part::reader(reader)
            .mime_str("application/octet-stream")
            .unwrap()
            .file_name(
                Path::new(file_path)
                    .file_name()
                    .unwrap()
                    .to_str()
                    .unwrap()
                    .to_string(),
            );
        //create the multipart form
        let form = multipart::Form::new().part("file", some_file);

        match client.post(url_str).multipart(form).send() {
            Ok(resp) => Some(serde_json::from_str(&resp.text().unwrap()).unwrap()),
            Err(err) => {
                eprintln!("Failed uploading file: {:?}", err);
                None
            }
        }
    }
}

#[cfg(feature = "reqwest")]
fn load_ei_data_from_gz(gz_filename: &str) -> Option<EliteInsightData> {
    match File::open(gz_filename) {
        Ok(file) => {
            let reader = BufReader::new(GzDecoder::new(file));
            match serde_json::from_reader(reader) {
                Ok(ei_data) => Some(ei_data),
                Err(e) => {
                    println!("JSON was not well-formatted for {}: {}", gz_filename, e);
                    None
                }
            }
        }
        Err(err) => {
            println!("{}", err);
            None
        }
    }
}

#[cfg(feature = "reqwest")]
fn load_ei_data_from_json(js_filename: &str) -> Option<EliteInsightData> {
    let file = File::open(js_filename).unwrap();
    let reader = BufReader::new(file);
    match serde_json::from_reader(reader) {
        Ok(x) => Some(x),
        Err(e) => {
            println!("JSON was not well-formatted {}", e);
            None
        }
    }
}

#[cfg(feature = "reqwest")]
fn write_ei_data_to_gz(gz_filename: &str, ei_data: &EliteInsightData) {
    println!("Creating {}", gz_filename);
    let file = File::create(gz_filename).unwrap();
    let mut writer = GzEncoder::new(file, Compression::default());
    let str = serde_json::to_string(&ei_data).expect("JSON was not well-formatted");
    writer.write_all(str.as_bytes()).unwrap();
    writer.finish().unwrap();
}

#[cfg(feature = "reqwest")]

pub fn get_ei_data_cached(url: &str, cache_path: Option<&str>) -> Option<EliteInsightData> {
    let dps_re =
        Regex::new(r"^https://dps\.report/[a-zA-z0-9]{4}-((\d{8}-\d{6})|(logfile))_[a-z]{2,5}$")
            .unwrap();
    let wvw_re =
        Regex::new(r"^https://wvw\.report/[a-zA-z0-9]{4}-\d{8}-\d{6}_[a-z]{2,5}$").unwrap();
    let arr;
    // wvw logs url like "https://wvw.report/hL2Y-20230201-205741_wvw"
    let _is_wvw = if dps_re.is_match(url) {
        arr = url.split("https://dps.report/").collect::<Vec<&str>>();
        false
    } else if wvw_re.is_match(url) {
        arr = url.split("https://wvw.report/").collect::<Vec<&str>>();
        true
    } else {
        println!("invalid url: \"{}\"", url);
        return None;
    };
    let url_id_part = arr.get(1)?;
    let mut j_fn = String::from(cache_path.unwrap_or(DEFAULT_CACHE_PATH));
    j_fn.push_str(url_id_part);
    j_fn.push_str(".json");
    let mut gz_fn = j_fn.clone();
    gz_fn.push_str(".gz");
    if Path::new(&gz_fn).exists() {
        if Path::new(&j_fn).exists() {
            println!("Deleting {} in favor of gz", j_fn);
            fs::remove_file(j_fn).unwrap();
        }
        let ei_data = load_ei_data_from_gz(&gz_fn);
        return ei_data;
    } else if Path::new(&j_fn).exists() {
        let ei_data = load_ei_data_from_json(&j_fn);
        if let Some(ei_data) = &ei_data {
            write_ei_data_to_gz(&gz_fn, ei_data);
        }
        return ei_data;
    } else {
        let api = DpsReportApi::default();
        let ei_data = api.get_json_data_by_link(&url.parse::<String>().unwrap());
        if ei_data.is_some() {
            match ei_data.unwrap() {
                DpsReportResult::Data(data) => {
                    write_ei_data_to_gz(&gz_fn, &data);
                    return Some(data);
                }
                DpsReportResult::Error(e) => {
                    eprintln!("Dps report api returned error: {}", e.error);
                    return None;
                }
            };
        }
    }
    None
}
