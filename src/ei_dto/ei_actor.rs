use std::sync::Arc;

use chrono::Duration;
use num_traits::clamp;
use serde::{Deserialize, Serialize};

use super::{
    ei_data_types::{
        EIActorCombatReplayData, EIDamageDist, EIDefenseAll, EIInstanceId, EIMinion, EIRotation,
        EIDPS,
    },
    ei_stats::EIGameplayStatsAll,
    helper_types::{DamageSource, DamageType, PhaseIndex, Pos2D},
};

pub trait EIActor {
    fn name(&self) -> &str;
    fn total_health(&self) -> Option<i32>;
    fn condition_rank(&self) -> u32;
    fn concentration_rank(&self) -> u32;
    fn healing_rank(&self) -> u32;
    fn toughness_rank(&self) -> u32;
    fn hitbox_height(&self) -> u32;
    fn hitbox_width(&self) -> u32;
    fn instance_id(&self) -> EIInstanceId;
    fn is_fake(&self) -> bool;
    fn get_total_damage(&self, damage_type: DamageType, damage_source: DamageSource) -> i32;
    fn get_damage_phase(
        &self,
        damage_type: DamageType,
        damage_source: DamageSource,
        phase_index: PhaseIndex,
    ) -> i32;
    fn get_total_dps(&self, damage_type: DamageType, damage_source: DamageSource) -> i32;
    fn get_dps_phase(
        &self,
        damage_type: DamageType,
        damage_source: DamageSource,
        phase_index: PhaseIndex,
    ) -> i32;
    fn get_total_breakbar_damage(&self, damage_source: DamageSource) -> f64;
    fn total_stats(&self) -> Option<&EIGameplayStatsAll>;
    fn stats_phase(&self, phase_index: PhaseIndex) -> Option<&EIGameplayStatsAll>;
    fn total_defense_stats(&self) -> Option<&EIDefenseAll>;
    fn get_damage_at_time(&self, damage_type: DamageType, time: Duration) -> Option<i32>;
    fn get_breakbar_damage_at_time(&self, time: Duration) -> Option<f64>;
    /*
     minions: Vec<EIMinion>,
    #[serde(default)]
    total_damage_dist: Vec<Vec<EIDamageDist>>,
    total_damage_taken: Vec<Vec<EIDamageDist>>,
     */
    fn get_skills_used(&self) -> Option<&Arc<[EIRotation]>>;

    fn get_condition_number_at_time(&self, time: Duration) -> i32;
    fn get_boon_number_at_time(&self, time: Duration) -> Option<i32>;
    fn get_minion_number_at_time(&self, time: Duration) -> i32;
    fn get_health_percent_at_time(&self, time: Duration) -> f64;
    fn get_barrier_percent_at_time(&self, time: Duration) -> Option<f64>;
    fn get_position_at_time(&self, time: Duration, poll_rate: i32) -> Option<Pos2D>;
    fn get_orientation_at_time(&self, time: Duration, poll_rate: i32) -> Option<f32>;
    fn is_dead_at_time(&self, time: Duration) -> Option<bool>;
    fn is_down_at_time(&self, time: Duration) -> Option<bool>;
    fn is_dc_at_time(&self, time: Duration) -> Option<bool>;
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub(crate) struct EIActorStruct {
    name: String,
    total_health: Option<i32>,
    #[serde(rename = "condition")]
    condition_rank: u32,
    #[serde(rename = "concentration")]
    concentration_rank: u32,
    #[serde(rename = "healing")]
    healing_rank: u32,
    #[serde(rename = "toughness")]
    toughness_rank: u32,
    hitbox_height: u32,
    hitbox_width: u32,
    #[serde(default)]
    #[serde(rename = "instanceID")]
    instance_id: u16,
    #[serde(default)]
    minions: Vec<EIMinion>,
    #[serde(default)]
    is_fake: bool,
    dps_all: Arc<[EIDPS]>,
    stats_all: Arc<[EIGameplayStatsAll]>,
    defenses: Arc<[EIDefenseAll]>,
    // todo type
    total_damage_dist: Vec<Vec<EIDamageDist>>,
    // todo type
    total_damage_taken: Vec<Vec<EIDamageDist>>,
    rotation: Option<Arc<[EIRotation]>>,
    #[serde(rename = "damage1S")]
    damage_1s: Vec<Vec<i32>>,
    #[serde(rename = "powerDamage1S")]
    power_damage_1s: Option<Vec<Vec<i32>>>,
    #[serde(rename = "conditionDamage1S")]
    condition_damage_1s: Option<Vec<Vec<i32>>>,
    #[serde(rename = "breakbarDamage1S")]
    breakbar_damage_1s: Option<Vec<Vec<f64>>>,
    #[serde(default)]
    conditions_states: Vec<[i32; 2]>,
    #[serde(default)]
    boons_states: Option<Arc<[[i32; 2]]>>,
    #[serde(default)]
    active_combat_minions: Vec<[i32; 2]>,
    health_percents: Arc<[[f64; 2]]>,
    barrier_percents: Option<Arc<[[f64; 2]]>>,
    combat_replay_data: Option<EIActorCombatReplayData>,
}

impl EIActor for EIActorStruct {
    /// Display name of the actor
    fn name(&self) -> &str {
        &self.name
    }
    /// Total maximum health of the actor or `None` if not available for this entity/in this version
    fn total_health(&self) -> Option<i32> {
        match self.total_health {
            Some(value) if value >= 0 => Some(value),
            _ => None,
        }
    }
    /// Rank of the condi damge of the actor
    fn condition_rank(&self) -> u32 {
        self.condition_rank
    }
    /// Rank of the concentration of the actor
    fn concentration_rank(&self) -> u32 {
        self.concentration_rank
    }
    /// Rank of the healing power of the actor
    fn healing_rank(&self) -> u32 {
        self.healing_rank
    }
    /// Rank of the toughness of the actor
    fn toughness_rank(&self) -> u32 {
        self.toughness_rank
    }
    /// Height of actor's hitbox in units
    fn hitbox_height(&self) -> u32 {
        self.hitbox_height
    }
    /// Height of actor's width/diameter in units
    fn hitbox_width(&self) -> u32 {
        self.hitbox_width
    }
    fn instance_id(&self) -> EIInstanceId {
        EIInstanceId(self.instance_id)
    }
    fn is_fake(&self) -> bool {
        self.is_fake
    }
    fn get_total_damage(&self, damage_type: DamageType, damage_source: DamageSource) -> i32 {
        match self.dps_all.first() {
            Some(eidps) => eidps.get_damage(damage_type, damage_source),
            None => 0,
        }
    }
    fn get_damage_phase(
        &self,
        damage_type: DamageType,
        damage_source: DamageSource,
        phase_index: PhaseIndex,
    ) -> i32 {
        match self.dps_all.get(phase_index.0) {
            Some(eidps) => eidps.get_damage(damage_type, damage_source),
            None => 0,
        }
    }
    fn get_total_dps(&self, damage_type: DamageType, damage_source: DamageSource) -> i32 {
        match self.dps_all.first() {
            Some(eidps) => eidps.get_dps(damage_type, damage_source),
            None => 0,
        }
    }
    fn get_dps_phase(
        &self,
        damage_type: DamageType,
        damage_source: DamageSource,
        phase_index: PhaseIndex,
    ) -> i32 {
        match self.dps_all.get(phase_index.0) {
            Some(eidps) => eidps.get_dps(damage_type, damage_source),
            None => 0,
        }
    }
    fn get_total_breakbar_damage(&self, damage_source: DamageSource) -> f64 {
        match self.dps_all.first() {
            Some(eidps) => eidps.get_breakbar_damage(damage_source),
            None => 0.0,
        }
    }
    fn total_stats(&self) -> Option<&EIGameplayStatsAll> {
        self.stats_all.first()
    }
    fn stats_phase(&self, phase_index: PhaseIndex) -> Option<&EIGameplayStatsAll> {
        self.stats_all.get(phase_index.0)
    }
    // todo phased ver
    fn total_defense_stats(&self) -> Option<&EIDefenseAll> {
        self.defenses.first()
    }
    fn get_damage_at_time(&self, damage_type: DamageType, time: Duration) -> Option<i32> {
        let arr = match damage_type {
            DamageType::Condi => self.condition_damage_1s.as_ref(),
            DamageType::Power => self.power_damage_1s.as_ref(),
            DamageType::Total => Some(&self.damage_1s),
        }?
        .first()?;
        let index = clamp(time.num_milliseconds() / 1000, 0, (arr.len() - 1) as i64);
        Some(*arr.get(index as usize)?)
    }
    fn get_breakbar_damage_at_time(&self, time: Duration) -> Option<f64> {
        let arr = self.breakbar_damage_1s.as_ref()?.first()?;
        let index = clamp(time.num_milliseconds() / 1000, 0, (arr.len() - 1) as i64);
        Some(*arr.get(index as usize)?)
    }

    fn get_skills_used(&self) -> Option<&Arc<[EIRotation]>> {
        self.rotation.as_ref()
    }

    fn get_condition_number_at_time(&self, time: Duration) -> i32 {
        let mut current = 0;
        for element in &self.conditions_states {
            if (element[0] as i64) < time.num_milliseconds() {
                current = element[1]
            } else {
                break;
            }
        }
        current
    }
    fn get_boon_number_at_time(&self, time: Duration) -> Option<i32> {
        let mut current = 0;
        for element in self.boons_states.as_ref()?.iter() {
            if (element[0] as i64) < time.num_milliseconds() {
                current = element[1]
            } else {
                break;
            }
        }
        Some(current)
    }
    fn get_minion_number_at_time(&self, time: Duration) -> i32 {
        let mut current = 0;
        for element in &self.active_combat_minions {
            if (element[0] as i64) < time.num_milliseconds() {
                current = element[1]
            } else {
                break;
            }
        }
        current
    }
    fn get_health_percent_at_time(&self, time: Duration) -> f64 {
        let mut current = 100.0;
        for element in self.health_percents.iter() {
            if element[0] < time.num_milliseconds() as f64 {
                current = element[1]
            } else {
                break;
            }
        }
        current
    }
    fn get_barrier_percent_at_time(&self, time: Duration) -> Option<f64> {
        let mut current = 0.0;
        for element in self.barrier_percents.as_ref()?.iter() {
            if element[0] < time.num_milliseconds() as f64 {
                current = element[1]
            } else {
                break;
            }
        }
        Some(current)
    }
    /// returns None if no combat replay data is present, it is empty or the provided time is negative
    /// returns last if time is past last combat replay entry
    /// otherwise interpolate between closesest entries
    fn get_position_at_time(&self, time: Duration, poll_rate: i32) -> Option<Pos2D> {
        self.combat_replay_data
            .as_ref()?
            .get_position_at_time(time.num_milliseconds(), poll_rate)
    }
    /// returns None if no combat replay data is present, it is empty or the provided time is negative
    /// returns last if time is past last combat replay entry
    /// otherwise interpolate between closesest entries
    fn get_orientation_at_time(&self, time: Duration, poll_rate: i32) -> Option<f32> {
        self.combat_replay_data
            .as_ref()?
            .get_orientation_at_time(time.num_milliseconds(), poll_rate)
    }
    /// returns None if no combat replay data is present
    fn is_dead_at_time(&self, time: Duration) -> Option<bool> {
        Some(
            self.combat_replay_data
                .as_ref()?
                .is_dead_at_time(time.num_milliseconds()),
        )
    }
    /// returns None if no combat replay data is present
    fn is_dc_at_time(&self, time: Duration) -> Option<bool> {
        Some(
            self.combat_replay_data
                .as_ref()?
                .is_dc_at_time(time.num_milliseconds()),
        )
    }
    /// returns None if no combat replay data is present
    fn is_down_at_time(&self, time: Duration) -> Option<bool> {
        Some(
            self.combat_replay_data
                .as_ref()?
                .is_down_at_time(time.num_milliseconds()),
        )
    }
}
