use std::collections::HashMap;
use std::fmt;
use std::sync::Arc;

use chrono::{DateTime, Duration, FixedOffset};
use num_derive::FromPrimitive;
use serde::de::Deserializer;
use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
use strum_macros::EnumIter;

use super::ei_actor::EIActor;
use super::ei_npc::EINPC;
use super::ei_player::EIPlayer;
use super::helper_types::{BuffId, DamageSource, DamageType, Pos2D};

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub(crate) struct EIDPS {
    dps: i32,
    damage: i32,
    condi_dps: i32,
    condi_damage: i32,
    power_dps: i32,
    power_damage: i32,
    breakbar_damage: f64,
    actor_dps: i32,
    actor_damage: i32,
    actor_condi_dps: i32,
    actor_condi_damage: i32,
    actor_power_dps: i32,
    actor_power_damage: i32,
    actor_breakbar_damage: f64,
}

impl EIDPS {
    pub(crate) fn get_damage(&self, damage_type: DamageType, damage_source: DamageSource) -> i32 {
        match (damage_type, damage_source) {
            (DamageType::Total, DamageSource::All) => self.damage,
            (DamageType::Total, DamageSource::ActorOnly) => self.actor_damage,
            (DamageType::Condi, DamageSource::All) => self.condi_damage,
            (DamageType::Condi, DamageSource::ActorOnly) => self.actor_condi_damage,
            (DamageType::Power, DamageSource::All) => self.power_damage,
            (DamageType::Power, DamageSource::ActorOnly) => self.actor_power_damage,
        }
    }

    pub(crate) fn get_dps(&self, damage_type: DamageType, damage_source: DamageSource) -> i32 {
        match (damage_type, damage_source) {
            (DamageType::Total, DamageSource::All) => self.dps,
            (DamageType::Total, DamageSource::ActorOnly) => self.actor_dps,
            (DamageType::Condi, DamageSource::All) => self.condi_dps,
            (DamageType::Condi, DamageSource::ActorOnly) => self.actor_condi_dps,
            (DamageType::Power, DamageSource::All) => self.power_dps,
            (DamageType::Power, DamageSource::ActorOnly) => self.actor_power_dps,
        }
    }
    pub(crate) fn get_breakbar_damage(&self, damage_source: DamageSource) -> f64 {
        match damage_source {
            DamageSource::All => self.breakbar_damage,
            DamageSource::ActorOnly => self.actor_breakbar_damage,
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct EIInstanceId(pub u16);

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
/// Support statistics
pub(crate) struct EIPlayerSupport {
    /// Number of time ressurected someone.
    pub(crate) resurrects: i64,
    /// Time passed on ressurecting.
    pub(crate) resurrect_time: f64,
    /// Number of times a condition was cleansed on a squad mate.
    pub(crate) condi_cleanse: i64,
    /// Total duration of condition cleansed on a squad mate.
    pub(crate) condi_cleanse_time: f64,
    /// Number of time a condition was cleansed from self.
    pub(crate) condi_cleanse_self: i64,
    /// Total duration of condition cleansed from self.
    pub(crate) condi_cleanse_time_self: f64,
    /// Number of time a boon was stripped.
    pub(crate) boon_strips: i64,
    /// Total duration of boons stripped from self.
    pub(crate) boon_strips_time: f64,
}

/// Defensive stats.
#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIDefenseAll {
    /// Total damage taken.
    pub damage_taken: i64,
    /// Total condition damage taken.
    ///
    /// only available in new EI versions
    pub condition_damage_taken: Option<i64>,
    /// Total power damage taken.
    ///
    /// only available in new EI versions
    pub power_damage_taken: Option<i64>,
    pub strike_damage_taken: Option<i64>,
    /// Total life leech damage taken.
    ///
    /// only available in new EI versions
    pub life_leech_damage_taken: Option<i64>,
    /// Total damage taken while downed.
    ///
    /// only available in new EI versions
    pub downed_damage_taken: Option<i64>,
    /// Total breakbar damage taken.
    pub breakbar_damage_taken: f64,
    /// Number of blocks.
    pub blocked_count: i32,
    /// Number of evades.
    pub evaded_count: i32,
    /// Number of misses.
    pub missed_count: i32,
    /// Number of dodges.
    pub dodge_count: i32,
    /// Number of time an incoming attack was negated by invul.
    pub invulned_count: i32,
    /// Damage done against barrier.
    pub damage_barrier: i32,
    /// Number of time interrupted.
    pub interrupted_count: i32,
    /// Number of time downed.
    pub down_count: i32,
    /// Time passed in downstate.
    down_duration: i64,
    /// Number of time died.
    pub dead_count: i32,
    /// Time passed in dead state.
    dead_duration: i64,
    /// Number of time disconnected.
    pub dc_count: i32,
    /// Time passed in disconnected state.
    dc_duration: i64,
    // todo new stats
    // condition_cleanses
    // condition_cleanses_time
}
impl EIDefenseAll {
    /// Time passed in downstate.
    pub fn down_duration(&self) -> Duration {
        Duration::milliseconds(self.down_duration)
    }
    /// Time passed in dead state.
    pub fn dead_duration(&self) -> Duration {
        Duration::milliseconds(self.dead_duration)
    }
    /// Time passed in disconnected state.
    pub fn dc_duration(&self) -> Duration {
        Duration::milliseconds(self.dc_duration)
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub(crate) struct EIBuffUptimeData {
    pub(crate) uptime: f64,
    presence: f64,
    #[serde(default)]
    generated: HashMap<String, f64>,
    #[serde(default)]
    overstacked: HashMap<String, f64>,
    #[serde(default)]
    wasted: HashMap<String, f64>,
    #[serde(default)]
    unknown_extended: HashMap<String, f64>,
    #[serde(default)]
    by_extension: HashMap<String, f64>,
    #[serde(default)]
    extended: HashMap<String, f64>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub(crate) struct EIBuffsUptime {
    pub(crate) id: i64,
    pub(crate) buff_data: Arc<[EIBuffUptimeData]>,
    states: Arc<[[i32; 2]]>,
    // newish
    states_per_source: Option<HashMap<String, Arc<[[i32; 2]]>>>,
}

impl EIBuffsUptime {
    pub(crate) fn get_count_at_time(&self, time: Duration) -> i32 {
        let mut last = 0;
        let time_ms = time.num_milliseconds();
        for [st_time, value] in self.states.iter() {
            if *st_time > time_ms as i32 {
                break;
            }
            last = *value;
        }
        last
    }
    pub(crate) fn get_final_count(&self) -> i32 {
        match self.states.last() {
            None => 0,
            Some([_time, value]) => *value,
        }
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIDamageDist {
    pub total_damage: i32,
    #[serde(default)]
    pub total_breakbar_damage: f64,
    min: i32,
    max: i32,
    hits: i32,
    pub connected_hits: i32,
    crit: i32,
    glance: i32,
    flank: i32,
    pub against_moving: Option<i32>,
    pub missed: i32,
    pub invulned: i32,
    pub interrupted: i32,
    pub evaded: i32,
    pub blocked: i32,
    pub shield_damage: i32,
    pub crit_damage: i32,
    pub id: i64,
    pub indirect_damage: bool,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
struct EISkill {
    cast_time: i32,
    duration: i32,
    time_gained: i32,
    quickness: f64,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIRotation {
    //TODO
    pub id: i64,
    //TODO
    skills: Arc<[EISkill]>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub(crate) struct EIMinion {
    pub name: String,
    #[serde(default)]
    pub id: Option<i32>,
    total_damage: Arc<[i32]>,
    // todo type
    total_target_damage: Option<Vec<Vec<i32>>>,
    total_breakbar_damage: Arc<[f64]>,
    // todo type
    total_target_breakbar_damage: Option<Vec<Vec<f64>>>,
    total_shield_damage: Arc<[i32]>,
    // todo type
    total_target_shield_damage: Option<Vec<Vec<i32>>>,
    // todo type
    total_damage_dist: Vec<Vec<EIDamageDist>>,
    // todo type
    target_damage_dist: Option<Vec<Vec<Vec<EIDamageDist>>>>,
    rotation: Option<Arc<[EIRotation]>>,
    // TODO EXTEIMinionHealingStats: EXTHealingStats
    // TODO EXTEIMinionBarrierStats: EXTBarrierStats
}

fn parse_vec_null_fix<'de, D>(d: D) -> Result<Vec<f32>, D::Error>
where
    D: Deserializer<'de>,
{
    Deserialize::deserialize(d).map(|x: Option<_>| x.unwrap_or_default())
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub(crate) struct EIActorCombatReplayData {
    pub(crate) start: i64,
    pub(crate) end: i64,
    #[serde(rename = "iconURL")]
    pub(crate) icon_url: String,
    pub(crate) positions: Arc<[[f32; 2]]>,
    #[serde(deserialize_with = "parse_vec_null_fix", default)]
    // if positions is empty orientation is not defined; this is null for some ca logs
    pub(crate) orientations: Vec<f32>,
    #[serde(default)]
    pub(crate) dead: Vec<[i64; 2]>,
    #[serde(default)]
    pub(crate) down: Vec<[i64; 2]>,
    #[serde(default)]
    pub(crate) dc: Vec<[i64; 2]>,
}

impl EIActorCombatReplayData {
    /// returns None if it is empty or the provided time is negative
    /// returns last if time is past last combat replay entry
    /// otherwise interpolate between closesest entries
    pub(crate) fn get_position_at_time(&self, time_ms: i64, poll_rate: i32) -> Option<Pos2D> {
        let ti0 = (time_ms - self.start) / poll_rate as i64;
        let ti1 = ti0 + 1;
        let x: f32;
        let y: f32;
        if ti1 >= self.positions.len() as i64 {
            x = self.positions.last()?[0];
            y = self.positions.last()?[1];
        } else {
            let ti0t = (self.start as f32 / poll_rate as f32).ceil() * poll_rate as f32
                + ti0 as f32 * poll_rate as f32;
            let w1: f32 = (time_ms as f32 - ti0t) / poll_rate as f32;
            let w0 = 1.0 - w1;
            let p0 = self.positions.get(ti0 as usize)?;
            let p1 = self.positions.get(ti1 as usize)?;
            x = w0 * p0[0] + w1 * p1[0];
            y = w0 * p0[1] + w1 * p1[1];
        }
        Some(Pos2D { x, y })
    }

    /// returns None if it is empty or the provided time is negative
    /// returns last if time is past last combat replay entry
    /// otherwise interpolate between closesest entries
    pub(crate) fn get_orientation_at_time(&self, time_ms: i64, poll_rate: i32) -> Option<f32> {
        let ti0 = (time_ms - self.start) / poll_rate as i64;
        let ti1 = ti0 + 1;
        if ti1 >= self.positions.len() as i64 {
            self.orientations.last().copied()
        } else {
            let ti0t = (self.start as f32 / poll_rate as f32).ceil() * poll_rate as f32
                + ti0 as f32 * poll_rate as f32;
            let w1: f32 = (time_ms as f32 - ti0t) / poll_rate as f32;
            let w0 = 1.0 - w1;
            let r0 = self.orientations.get(ti0 as usize)?;
            let r1 = self.orientations.get(ti1 as usize)?;
            Some(w0 * r0 + w1 * r1)
        }
    }
    pub(crate) fn is_dead_at_time(&self, time_ms: i64) -> bool {
        for element in &self.dead {
            if element[0] < time_ms && element[1] > time_ms {
                return true;
            }
        }
        false
    }

    pub(crate) fn is_dc_at_time(&self, time_ms: i64) -> bool {
        for element in &self.dc {
            if element[0] < time_ms && element[1] > time_ms {
                return true;
            }
        }
        false
    }

    pub(crate) fn is_down_at_time(&self, time_ms: i64) -> bool {
        for element in &self.down {
            if element[0] < time_ms && element[1] > time_ms {
                return true;
            }
        }
        false
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EISkillDesc {
    pub name: String,
    pub auto_attack: bool,
    pub can_crit: bool,
    pub icon: Option<String>,
    pub is_swap: bool,
    #[serde(default)] // TODO better default ???
    pub is_instant_cast: bool,
    pub is_not_accurate: bool,
    pub conversion_based_healing: Option<bool>,
    pub hybrid_healing: Option<bool>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIBuffDesc {
    pub name: String,
    icon: Option<String>,
    stacking: bool,
    #[serde(default)]
    pub descriptions: Vec<String>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIDamageModDesc {
    pub name: String,
    pub icon: String,
    pub description: String,
    pub non_multiplier: bool,
    pub skill_based: bool,
    #[serde(default)]
    pub approximate: bool,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIDamageModifierItem {
    pub hit_count: i32,
    pub total_hit_count: i32,
    pub damage_gain: f64,
    pub total_damage: i32,
}

impl EIDamageModifierItem {
    pub fn get_uptime_hits(&self) -> f64 {
        self.hit_count as f64 / self.total_hit_count as f64
    }
    pub fn get_relative_damage_gain(&self) -> f64 {
        self.damage_gain / self.total_damage as f64
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIDamageModifierData {
    pub(crate) id: i32,
    pub(crate) damage_modifiers: Arc<[EIDamageModifierItem]>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub(crate) struct EIBuffsGenerationData {
    pub(crate) generation: f64,
    pub(crate) overstack: f64,
    pub(crate) wasted: f64,
    pub(crate) unknown_extended: f64,
    pub(crate) by_extension: f64,
    pub(crate) extended: f64,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIPlayerBuffsGeneration {
    pub(crate) id: i64,
    pub(crate) buff_data: Arc<[EIBuffsGenerationData]>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIConsumable {
    stack: i32,
    duration: i32,
    time: i64,
    id: i64,
}

impl EIConsumable {
    // todo
}

#[derive(Debug, Deserialize, Serialize, Clone, Copy, PartialEq, Hash)]
pub enum EIWeaponType {
    // two-hand
    Greatsword,
    Hammer,
    Longbow,
    Rifle,
    Shortbow,
    Staff,
    // one-hand
    Axe,
    Dagger,
    Mace,
    Pistol,
    Sword,
    Scepter,
    // off-hand
    Focus,
    Shield,
    Torch,
    Warhorn,
    // underwater
    Harpoongun,
    // todo confirm this value exits
    Speargun,
    // inconsistent // this exists
    Spear,
    Trident,
    // other
    #[serde(rename = "2Hand")]
    TwoHand,
    Unknown,
}

impl fmt::Display for EIWeaponType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIPhase {
    start: i64,
    end: i64,
    pub name: String,
    pub targets: Arc<[usize]>,
    #[serde(default)]
    pub sub_phases: Vec<usize>,
    pub breakbar_phase: bool,
}

impl EIPhase {
    pub fn get_start(&self) -> Duration {
        Duration::milliseconds(self.start)
    }
    pub fn get_end(&self) -> Duration {
        Duration::milliseconds(self.end)
    }
    pub fn get_duration(&self) -> Duration {
        Duration::milliseconds(self.end - self.start)
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIMechanicInstance {
    time: i64,
    // todo time in ms!
    pub actor: String,
}

impl EIMechanicInstance {
    pub fn get_time(&self) -> Duration {
        Duration::milliseconds(self.time)
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIMechanic {
    pub mechanics_data: Arc<[EIMechanicInstance]>,
    pub name: String,
    pub description: String,
}

impl EIMechanic {
    pub fn get_actor_instances(&self, actor_name: &str) -> Arc<[&EIMechanicInstance]> {
        return self
            .mechanics_data
            .iter()
            .filter(|&inst| inst.actor == actor_name)
            .collect();
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIExtensionDesc {
    pub name: String,
    pub version: String,
    pub revision: u32,
    pub signature: u32,
    #[serde(default)]
    pub running_extension: Vec<String>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EICombatReplayMap {
    pub url: String,
    interval: [i64; 2], //todo what does that actually do
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EICombatReplayMetaData {
    pub inch_to_pixel: f32,
    pub polling_rate: i32,
    pub sizes: [i32; 2],
    pub maps: Vec<EICombatReplayMap>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(untagged)]
pub enum EliteInsightOrDPSReportError {
    Data(EliteInsightData),
    Error(DPSReportError),
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct DPSReportError {
    pub error: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
/// The root of the JSON.
pub struct EliteInsightData {
    /// The used EI version.
    elite_insights_version: String,
    #[serde(rename = "triggerID")]
    /// The id with which the log has been triggered.
    trigger_id: i32,
    #[serde(rename = "eiEncounterID")]
    /// The elite insight id of the log, indicates which encounter the log corresponds to. See <https://github.com/baaron4/GW2-Elite-Insights-Parser/blob/master/EncounterIDs.md/>. Only present in newer log version.
    ei_encounter_id: Option<EIEncounterID>,
    fight_name: String,
    fight_icon: String,
    arc_version: String,
    #[serde(rename = "gW2Build")]
    gw2_build: u64,
    language: String,
    fractal_scale: Option<i32>,
    #[serde(rename = "languageID")]
    language_id: u8,
    recorded_by: String,
    time_start: String,
    time_end: String,
    #[serde(with = "std_date_format")]
    time_start_std: DateTime<FixedOffset>,
    #[serde(with = "std_date_format")]
    time_end_std: DateTime<FixedOffset>,
    #[serde(with = "log_duration_format")]
    duration: Duration,
    log_start_offset: Option<i64>, // todo ms
    success: bool,
    #[serde(rename = "isCM")]
    is_cm: bool,
    #[serde(default, rename = "isLegendaryCM")]
    is_legendary_cm: bool,
    is_late_start: Option<bool>,
    missing_pre_event: Option<bool>,
    #[serde(default)]
    anonymous: bool,
    #[serde(default, rename = "detailedWvW")]
    is_detailed_wvw: bool,
    pub targets: Arc<[EINPC]>,
    pub players: Arc<[EIPlayer]>,
    pub phases: Arc<[EIPhase]>,
    pub mechanics: Option<Arc<[EIMechanic]>>,
    pub upload_links: Arc<[String]>,
    skill_map: HashMap<String, EISkillDesc>,
    buff_map: HashMap<String, EIBuffDesc>,
    damage_mod_map: HashMap<String, EIDamageModDesc>,
    personal_buffs: HashMap<String, Arc<[i64]>>,
    #[serde(default)]
    present_fractal_instabilities: Vec<i64>,
    #[serde(default)]
    pub present_instance_buffs: Vec<[i64; 2]>,
    #[serde(default)]
    pub log_errors: Vec<String>,
    pub used_extensions: Option<Arc<[EIExtensionDesc]>>,
    pub combat_replay_meta_data: Option<EICombatReplayMetaData>,
}

impl PartialEq for EliteInsightData {
    fn eq(&self, other: &Self) -> bool {
        self.time_start_std == other.time_start_std
            && self.time_end_std == other.time_end_std
            && self.recorded_by == other.recorded_by
    }
}

impl EliteInsightData {
    /// The used EliteInsight version.
    pub fn elite_insights_version(&self) -> &str {
        &self.elite_insights_version
    }
    /// The guild wars' id of the entity with which the log has been triggered.
    pub fn trigger_id(&self) -> i32 {
        self.trigger_id
    }
    /// The elite insight id of the log, indicates which encounter the log corresponds to.
    /// See <https://github.com/baaron4/GW2-Elite-Insights-Parser/blob/master/EncounterIDs.md/>.
    /// Only present in newer log version, `None` in older versions.
    pub fn ei_encounter_id(&self) -> Option<EIEncounterID> {
        self.ei_encounter_id
    }
    /// Name of the fight
    pub fn fight_name(&self) -> &str {
        &self.fight_name
    }
    /// URL to the icon of the fight
    pub fn fight_icon(&self) -> &str {
        &self.fight_icon
    }
    /// arcdps version used to generate the evtc log file
    pub fn arc_version(&self) -> &str {
        &self.arc_version
    }
    /// gw2 build number used when generating the evtc log file
    pub fn gw2_build(&self) -> u64 {
        self.gw2_build
    }
    /// Name of the language with which the evtc was generated. Note that language changes during the log might lead to mixed language log files
    pub fn language(&self) -> &str {
        &self.language
    }
    /// fractal scale of the fight if applicable, `None` if not a fractal or to old version to support fractal scale
    pub fn fractal_scale(&self) -> Option<i32> {
        self.fractal_scale
    }
    /// numeric id of the language used, 0 is english
    pub fn language_id(&self) -> u8 {
        self.language_id
    }
    /// Return a reference to the player recording this log file.
    /// # Panics
    /// If the recording player name could not be found within the data
    pub fn get_recording_player(&self) -> &EIPlayer {
        self.players
            .iter()
            .find(|&p| p.name() == self.recorded_by)
            .unwrap()
    }
    /// Time the fight started, will be epoch if not aplicable
    pub fn time_start(&self) -> DateTime<FixedOffset> {
        self.time_start_std
    }
    /// Time the log file ended, will be epoch if not aplicable
    pub fn time_end(&self) -> DateTime<FixedOffset> {
        self.time_end_std
    }
    /// Duration of the fight, milliseconds precision
    pub fn duration(&self) -> Duration {
        self.duration
    }
    /// Offset between fight start and log start, only present with newer elite insight versions
    pub fn log_start_offset(&self) -> Option<Duration> {
        self.log_start_offset.map(Duration::milliseconds)
    }
    /// Success status of the fight, `true` indicates a successfull kill.  
    pub fn success(&self) -> bool {
        self.success
    }
    /// If the fight is in challenge mode.
    pub fn is_cm(&self) -> bool {
        self.is_cm
    }
    /// If the fight is in legendary challenge mode. If this is true, `is_cm()` will also be true.
    pub fn is_legendary_cm(&self) -> bool {
        self.is_legendary_cm
    }
    /// True if EI detected that the encounter started later than expected.
    /// This value being false does not mean the encounter could not have started later than expected.
    /// This is also false if the version of EI used could not detect late starts yet.
    pub fn is_late_start(&self) -> bool {
        self.is_late_start.unwrap_or_default()
    }
    /// True if an encounter that is supposed to have a pre-event does not have it. Only present in newer EI versions, otherwise `None`
    pub fn missing_pre_event(&self) -> Option<bool> {
        self.missing_pre_event
    }
    /// If the log was parsed in anonymous mode. In this case player names, account names and guild ids are obfuscated.
    pub fn anonymous(&self) -> bool {
        self.anonymous
    }
    /// For WvW logs wether it was parsed in detailed mode. `false` if not a WvW log.  
    pub fn is_detailed_wvw(&self) -> bool {
        self.is_detailed_wvw
    }

    /// List of present fractal instabilities, the values are buff ids. DEPRECATED: use `present_instance_buffs()` instead.
    pub fn present_fractal_instabilities(&self) -> Vec<BuffId> {
        self.present_fractal_instabilities
            .iter()
            .map(|id| BuffId(*id))
            .collect()
    }

    pub fn get_mechanic_by_name(&self, name: &str) -> Option<&EIMechanic> {
        match &self.mechanics {
            None => None,
            Some(mech) => mech.iter().find(|&m| m.name == name),
        }
    }
    pub fn get_skill_by_id(&self, id: i64) -> Option<&EISkillDesc> {
        let id_str = format!("s{}", id);
        return self.skill_map.get(&*id_str);
    }
    pub fn get_buff_by_id(&self, id: BuffId) -> Option<&EIBuffDesc> {
        let id_str = format!("b{}", id.0);
        return self.buff_map.get(&*id_str);
    }
    pub fn get_damage_mod_by_id(&self, id: i64) -> Option<&EIDamageModDesc> {
        let id_str = format!("d{}", id);
        return self.damage_mod_map.get(&*id_str);
    }

    pub fn get_mechanic_counts(
        &self,
        mechanic_name: &str,
        before_ms: Option<i64>,
    ) -> HashMap<String, u32> {
        let mut mechanic_counts: HashMap<String, u32> = HashMap::new();
        if let Some(mechanics) = &self.mechanics {
            for mechanic in mechanics.iter() {
                if mechanic.name == mechanic_name {
                    for instance in mechanic.mechanics_data.iter() {
                        if before_ms.is_none() || before_ms.unwrap() > instance.time {
                            *mechanic_counts.entry(instance.actor.clone()).or_default() += 1;
                        }
                    }
                    break;
                }
            }
        }
        mechanic_counts
    }
}

#[derive(
    EnumIter,
    FromPrimitive,
    Serialize_repr,
    Deserialize_repr,
    PartialEq,
    Debug,
    Clone,
    Copy,
    Hash,
    Eq,
)]
#[repr(i32)]
pub enum EIEncounterID {
    Unknown = 0,
    /// Unsupported encounters are safe to ignore.
    Unsupported = 0x010000,
    ValeGuardian = 0x020101,
    Gorseval = 0x020102,
    Sabetha = 0x020103,
    Slothasor = 0x020201,
    BanditTrio = 0x020202,
    Matthias = 0x020203,
    Escort = 0x020301,
    KeepConstruct = 0x020302,
    TwistedCastle = 0x020303,
    Xera = 0x020304,
    Cairn = 0x020401,
    MursaatOverseer = 0x020402,
    Samarog = 0x020403,
    Deimos = 0x020404,
    SoullesHorror = 0x020501,
    RiverOfSouls = 0x020502,
    BrokenKing = 0x020503,
    SoulEater = 0x020504,
    EyesOfJudgementAndFate = 0x020505,
    Dhuum = 0x020506,
    ConjuredAmalgamate = 0x020601,
    TwinLargos = 0x020602,
    Qadim = 0x020603,
    Adina = 0x020701,
    Sabir = 0x020702,
    QadimThePeerless = 0x020703,
    Greer = 0x020801,
    Decima = 0x020802,
    Ura = 0x020803,
    MAMA = 0x030101,
    Siax = 0x030102,
    Ensolyss = 0x030103,
    Skorvald = 0x030201,
    Artsariiv = 0x030202,
    Arkk = 0x030203,
    AiElementalAndDark = 0x030301,
    AiElementalOnly = 0x030302,
    AiDarkOnly = 0x030303,
    Kanaxai = 0x030401,
    Freezie = 0x040101,
    IcebroodConstruct = 0x040201,
    FraenirOfJormag = 0x040202,
    VoiceAndClaw = 0x040203,
    Boneskinner = 0x040204,
    WhisperOfJormag = 0x040205,
    VariniaStormsounder = 0x040206,
    AetherbladeHideout = 0x040301,
    XunlaiJadeJunkyard = 0x040302,
    KainengOverlook = 0x040303,
    HarvestTemple = 0x040304,
    OldLionsCourt = 0x040305,
    CosmicObservatory = 0x040501,
    TempleOfFebe = 0x040502,
    SooWon = 0x050401,
    Mordremoth = 0x060201,
    EternalBattlegrounds = 0x070100,
    GreenAlpineBorderlands = 0x070200,
    BlueAlpineBorderlands = 0x070300,
    RedDesertBorderlands = 0x070400,
    ObsidianSanctum = 0x070500,
    EdgeOfTheMists = 0x070600,
    ArmisticeBastion = 0x070700,
    GildedHollow = 0x070800,
    LostPrecipice = 0x070900,
    WindsweptHaven = 0x070A00,
    IsleOfReflection = 0x070B00,
    MassiveGolem10M = 0x080101,
    MassiveGolem4M = 0x080102,
    MassiveGolem1M = 0x080103,
    VitalGolem = 0x080104,
    AverageGolem = 0x080105,
    StandardGolem = 0x080106,
    ConditionGolem = 0x080107,
    PowerGolem = 0x080108,
    LargeGolem = 0x080109,
    MediumGolem = 0x08010A,
}

mod std_date_format {
    use chrono::{DateTime, FixedOffset};
    use serde::{self, Deserialize, Deserializer, Serializer};

    const FORMAT: &str = "%Y-%m-%d %H:%M:%S %:z";

    pub fn serialize<S>(date: &DateTime<FixedOffset>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let s = format!("{}", date.format(FORMAT));
        serializer.serialize_str(&s)
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<DateTime<FixedOffset>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        DateTime::parse_from_str(&s, FORMAT).map_err(serde::de::Error::custom)
    }
}

mod log_duration_format {
    use chrono::Duration;
    use regex::Regex;
    use serde::{self, Deserialize, Deserializer, Serializer};

    // "02m 23s 180ms"
    //const FORMAT: &'static str = "%Mm %Ss %3fms";

    pub fn serialize<S>(duration: &Duration, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let secs = duration.num_seconds();
        let ms = duration.num_milliseconds() - secs * 1000;
        let s = format!("{}m {}s {}ms", secs / 60, secs % 60, ms);
        serializer.serialize_str(&s)
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<Duration, D::Error>
    where
        D: Deserializer<'de>,
    {
        let re: Regex = Regex::new(r"^(?P<min>\d+)m (?P<sec>\d+)s (?P<ms>\d+)ms$").unwrap();
        let s = String::deserialize(deserializer)?;
        let cap = re.captures(&s).unwrap();
        let min = cap.get(1).unwrap().as_str().parse::<i64>().unwrap();
        let sec = cap.get(2).unwrap().as_str().parse::<i64>().unwrap();
        let ms = cap.get(3).unwrap().as_str().parse::<i64>().unwrap();
        Ok(Duration::milliseconds((min * 60 + sec) * 1000 + ms))
    }
}
