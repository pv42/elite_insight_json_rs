use std::sync::Arc;

use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIEXTPlayerHealingStats {
    // todo slice
    outgoing_healing_allies: Vec<Vec<EIEXTOutgoginHealingStatistics>>,
    outgoing_healing: Arc<[EIEXTOutgoginHealingStatistics]>,
    incoming_healing: Arc<[EIEXTIncomingHealingStatistics]>,
    #[serde(rename = "alliedHealing1S")]
    // todo slice
    allied_healing_1s: Vec<Vec<Vec<i32>>>,
    #[serde(rename = "alliedHealingPowerHealing1S")]
    // todo slice
    allied_healing_power_healing_1s: Vec<Vec<Vec<i32>>>,
    #[serde(rename = "alliedConversionHealingHealing1S")]
    // todo slice
    allied_conversion_healing_healing_1s: Vec<Vec<Vec<i32>>>,
    #[serde(rename = "alliedHybridHealing1S")]
    // todo slice
    allied_hybrid_healing_1s: Vec<Vec<Vec<i32>>>,
    #[serde(rename = "healing1S")]
    // todo slice
    healing_1s: Vec<Vec<i32>>,
    #[serde(rename = "healingPowerHealing1S")]
    // todo slice
    healing_power_healing_1s: Vec<Vec<i32>>,
    #[serde(rename = "conversionHealingHealing1S")]
    // todo slice
    conversion_healing_healing_1s: Vec<Vec<i32>>,
    #[serde(rename = "hybridHealing1S")]
    hybrid_healing_1s: Vec<Vec<i32>>,
    // todo slice
    allied_healing_dist: Vec<Vec<Vec<EIEXTHealingDist>>>,
    // todo slice
    total_healing_dist: Vec<Vec<EIEXTHealingDist>>,
    // todo slice
    total_incoming_healing_dist: Vec<Vec<EIEXTHealingDist>>,
}

impl EIEXTPlayerHealingStats {
    // TODO
    /// panics if called with `HealingKind::Downed`
    fn allied_hps_at_time(&self, healing_kind: HealingKind, ally_index: i32, time_ms: i32) -> i32 {
        let arr = match healing_kind {
            HealingKind::Total => &self.allied_healing_1s,
            HealingKind::HealingPower => &self.allied_healing_power_healing_1s,
            HealingKind::Conversion => &self.allied_conversion_healing_healing_1s,
            HealingKind::Hybrid => &self.allied_hybrid_healing_1s,
            HealingKind::Downed => panic!("Downed is not supported for allied healing at time"),
        };
        // select ally index
        let arr = match arr.get(ally_index as usize) {
            Some(arr) => arr,
            None => return 0,
        };
        // 1st phase
        let arr = match arr.first() {
            Some(arr) => arr,
            None => return 0,
        };
        let index = (time_ms / 1000) as usize;
        let before = arr.get(index).unwrap_or(&0);
        let after = arr.get(index + 1).unwrap_or(before);
        after - before
    }
    /// panics if called with `HealingKind::Downed`
    fn hps_at_time(&self, healing_kind: HealingKind, time_ms: i32) -> i32 {
        let arr = match healing_kind {
            HealingKind::Total => &self.healing_1s,
            HealingKind::HealingPower => &self.healing_power_healing_1s,
            HealingKind::Conversion => &self.conversion_healing_healing_1s,
            HealingKind::Hybrid => &self.hybrid_healing_1s,
            HealingKind::Downed => panic!("Downed is not supported for healing at time"),
        };
        // 1st phase
        let arr = match arr.first() {
            Some(arr) => arr,
            None => return 0,
        };
        let index = (time_ms / 1000) as usize;
        let before = arr.get(index).unwrap_or(&0);
        let after = arr.get(index + 1).unwrap_or(before);
        after - before
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIEXTOutgoginHealingStatistics {
    hps: i32,
    healing: i32,
    healing_power_hps: i32,
    healing_power_healing: i32,
    conversion_hps: i32,
    conversion_healing: i32,
    hybrid_hps: i32,
    hybrid_healing: i32,
    downed_hps: i32,
    downed_healing: i32,
    actor_hps: i32,
    actor_healing: i32,
    actor_healing_power_hps: i32,
    actor_healing_power_healing: i32,
    actor_conversion_hps: i32,
    actor_conversion_healing: i32,
    actor_hybrid_hps: i32,
    actor_hybrid_healing: i32,
    actor_downed_hps: i32,
    actor_downed_healing: i32,
}

pub enum HealingKind {
    Total,
    HealingPower,
    Conversion,
    Hybrid,
    Downed,
}

impl EIEXTOutgoginHealingStatistics {
    pub fn get_hps(&self, kind: HealingKind, actor_only: bool) -> i32 {
        match (kind, actor_only) {
            (HealingKind::Total, true) => self.actor_hps,
            (HealingKind::Total, false) => self.hps,
            (HealingKind::HealingPower, true) => self.actor_healing_power_hps,
            (HealingKind::HealingPower, false) => self.healing_power_hps,
            (HealingKind::Conversion, true) => self.actor_conversion_hps,
            (HealingKind::Conversion, false) => self.conversion_hps,
            (HealingKind::Hybrid, true) => self.actor_hybrid_hps,
            (HealingKind::Hybrid, false) => self.hybrid_hps,
            (HealingKind::Downed, true) => self.actor_downed_hps,
            (HealingKind::Downed, false) => self.downed_hps,
        }
    }
    pub fn get_healing(&self, kind: HealingKind, actor_only: bool) -> i32 {
        match (kind, actor_only) {
            (HealingKind::Total, true) => self.actor_healing,
            (HealingKind::Total, false) => self.healing,
            (HealingKind::HealingPower, true) => self.actor_healing_power_healing,
            (HealingKind::HealingPower, false) => self.healing_power_healing,
            (HealingKind::Conversion, true) => self.actor_conversion_healing,
            (HealingKind::Conversion, false) => self.conversion_healing,
            (HealingKind::Hybrid, true) => self.actor_hybrid_healing,
            (HealingKind::Hybrid, false) => self.hybrid_healing,
            (HealingKind::Downed, true) => self.actor_downed_healing,
            (HealingKind::Downed, false) => self.downed_healing,
        }
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIEXTIncomingHealingStatistics {
    healed: i32,
    healing_power_healed: i32,
    conversion_healed: i32,
    hybrid_healed: i32,
    downed_healed: i32,
}

impl EIEXTIncomingHealingStatistics {
    pub fn get_healed(&self, kind: HealingKind) -> i32 {
        match kind {
            HealingKind::Total => self.healed,
            HealingKind::HealingPower => self.healing_power_healed,
            HealingKind::Conversion => self.conversion_healed,
            HealingKind::Hybrid => self.hybrid_healed,
            HealingKind::Downed => self.downed_healed,
        }
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIEXTHealingDist {
    pub total_healing: i32,
    pub total_downed_healing: i32,
    pub min: i32,
    pub max: i32,
    pub hits: i32,
    pub id: i64, // TODO type
    pub indirect_healing: bool,
}
