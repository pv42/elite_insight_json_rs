use std::sync::Arc;

use chrono::Duration;
use serde::{Deserialize, Deserializer, Serialize};

use super::{
    ei_actor::{EIActor, EIActorStruct},
    ei_data_types::{EIBuffsUptime, EIDefenseAll, EIInstanceId, EIRotation},
    ei_stats::EIGameplayStatsAll,
    helper_types::{DamageSource, DamageType, PhaseIndex, Pos2D},
};

fn deserialize_vec_f64x2_maybe_nan<'de, D>(deserializer: D) -> Result<Vec<[f64; 2]>, D::Error>
where
    D: Deserializer<'de>,
{
    // we define a local enum type inside of the function
    // because it is untagged, serde will deserialize as the first variant
    // that it can
    #[derive(Deserialize)]
    #[serde(untagged)]
    enum MaybeNaN<U> {
        // if it can be parsed as Option<T>, it will be
        Value(U),
        // otherwise try parsing as a string
        NaNString(String),
    }
    let value: Vec<[MaybeNaN<f64>; 2]> = Deserialize::deserialize(deserializer)?;
    let mut result = Vec::<[f64; 2]>::new();
    for entry in value {
        let v0 = match entry.first().unwrap() {
            MaybeNaN::Value(value) => *value,
            MaybeNaN::NaNString(string) => {
                if string == "NaN" || string == "null" {
                    f64::NAN
                } else {
                    return Err(serde::de::Error::custom("Unexpected string"));
                }
            }
        };
        let v1 = match entry.get(1).unwrap() {
            MaybeNaN::Value(value) => *value,
            MaybeNaN::NaNString(string) => {
                if string == "NaN" {
                    f64::NAN
                } else {
                    return Err(serde::de::Error::custom("Unexpected string"));
                }
            }
        };
        result.push([v0, v1]);
    }
    Ok(result)
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EINPC {
    id: i32,
    final_health: i64,
    final_barrier: Option<i64>,
    barrier_percent: Option<f64>,
    health_percent_burned: f64,
    first_aware: i32,
    last_aware: i32,
    buffs: Arc<[EIBuffsUptime]>,
    #[serde(default)]
    enemy_player: bool,
    // TODO serialize with
    #[serde(deserialize_with = "deserialize_vec_f64x2_maybe_nan")]
    breakbar_percents: Vec<[f64; 2]>,
    // everything after here is inhereted from Actor
    #[serde(flatten)]
    ei_actor: EIActorStruct,
}

impl EIActor for EINPC {
    fn name(&self) -> &str {
        self.ei_actor.name()
    }
    /// Total HP of the NPC or `None` on old versions
    fn total_health(&self) -> Option<i32> {
        self.ei_actor.total_health()
    }
    fn condition_rank(&self) -> u32 {
        self.ei_actor.condition_rank()
    }
    fn concentration_rank(&self) -> u32 {
        self.ei_actor.concentration_rank()
    }
    fn healing_rank(&self) -> u32 {
        self.ei_actor.healing_rank()
    }
    fn toughness_rank(&self) -> u32 {
        self.ei_actor.toughness_rank()
    }
    fn hitbox_height(&self) -> u32 {
        self.ei_actor.hitbox_height()
    }
    fn hitbox_width(&self) -> u32 {
        self.ei_actor.hitbox_width()
    }
    fn instance_id(&self) -> EIInstanceId {
        self.ei_actor.instance_id()
    }
    fn is_fake(&self) -> bool {
        self.ei_actor.is_fake()
    }
    fn get_total_damage(&self, damage_type: DamageType, damage_source: DamageSource) -> i32 {
        self.ei_actor.get_total_damage(damage_type, damage_source)
    }
    fn get_damage_phase(
        &self,
        damage_type: DamageType,
        damage_source: DamageSource,
        phase_index: PhaseIndex,
    ) -> i32 {
        self.ei_actor
            .get_damage_phase(damage_type, damage_source, phase_index)
    }
    fn get_total_dps(&self, damage_type: DamageType, damage_source: DamageSource) -> i32 {
        self.ei_actor.get_total_dps(damage_type, damage_source)
    }
    fn get_dps_phase(
        &self,
        damage_type: DamageType,
        damage_source: DamageSource,
        phase_index: PhaseIndex,
    ) -> i32 {
        self.ei_actor
            .get_dps_phase(damage_type, damage_source, phase_index)
    }
    fn get_total_breakbar_damage(&self, damage_source: DamageSource) -> f64 {
        self.ei_actor.get_total_breakbar_damage(damage_source)
    }
    fn total_stats(&self) -> Option<&EIGameplayStatsAll> {
        self.ei_actor.total_stats()
    }
    fn stats_phase(&self, phase_index: PhaseIndex) -> Option<&EIGameplayStatsAll> {
        self.ei_actor.stats_phase(phase_index)
    }
    fn total_defense_stats(&self) -> Option<&EIDefenseAll> {
        self.ei_actor.total_defense_stats()
    }
    fn get_damage_at_time(&self, damage_type: DamageType, time: Duration) -> Option<i32> {
        self.ei_actor.get_damage_at_time(damage_type, time)
    }
    fn get_breakbar_damage_at_time(&self, time: Duration) -> Option<f64> {
        self.ei_actor.get_breakbar_damage_at_time(time)
    }
    fn get_skills_used(&self) -> Option<&Arc<[EIRotation]>> {
        self.ei_actor.get_skills_used()
    }
    fn get_condition_number_at_time(&self, time: Duration) -> i32 {
        self.ei_actor.get_condition_number_at_time(time)
    }
    fn get_boon_number_at_time(&self, time: Duration) -> Option<i32> {
        self.ei_actor.get_boon_number_at_time(time)
    }
    fn get_minion_number_at_time(&self, time: Duration) -> i32 {
        self.ei_actor.get_minion_number_at_time(time)
    }
    fn get_health_percent_at_time(&self, time: Duration) -> f64 {
        self.ei_actor.get_health_percent_at_time(time)
    }
    fn get_barrier_percent_at_time(&self, time: Duration) -> Option<f64> {
        self.ei_actor.get_barrier_percent_at_time(time)
    }

    /// returns None if no combat replay data is present, it is empty or the provided time is negative
    /// returns last if time is past last combat replay entry
    /// otherwise interpolate between closesest entries
    fn get_position_at_time(&self, time: Duration, poll_rate: i32) -> Option<Pos2D> {
        self.ei_actor.get_position_at_time(time, poll_rate)
    }

    /// returns None if no combat replay data is present, it is empty or the provided time is negative
    /// returns last if time is past last combat replay entry
    /// otherwise interpolate between closesest entries
    fn get_orientation_at_time(&self, time: Duration, poll_rate: i32) -> Option<f32> {
        self.ei_actor.get_orientation_at_time(time, poll_rate)
    }
    /// returns None if no combat replay data is present
    fn is_dead_at_time(&self, time: Duration) -> Option<bool> {
        self.ei_actor.is_dead_at_time(time)
    }
    /// returns None if no combat replay data is present
    fn is_dc_at_time(&self, time: Duration) -> Option<bool> {
        self.ei_actor.is_dc_at_time(time)
    }
    /// returns None if no combat replay data is present
    fn is_down_at_time(&self, time: Duration) -> Option<bool> {
        self.ei_actor.is_down_at_time(time)
    }
}

impl EINPC {
    pub fn id(&self) -> i32 {
        self.id
    }
    pub fn final_health(&self) -> i64 {
        self.final_health
    }
    pub fn final_barrier(&self) -> Option<i64> {
        self.final_barrier
    }
    pub fn barrier_percent(&self) -> Option<f64> {
        self.barrier_percent
    }
    pub fn health_percent_burned(&self) -> f64 {
        self.health_percent_burned
    }
    // todo time
    pub fn first_aware_ms(&self) -> i32 {
        self.first_aware
    }
    pub fn last_aware_ms(&self) -> i32 {
        self.last_aware
    }
    pub fn get_buff_uptime_total(&self, buff_id: i64) -> f64 {
        self.get_buff_uptime_phase(buff_id, PhaseIndex::full_fight())
    }
    pub fn get_buff_uptime_phase(&self, buff_id: i64, phase_index: PhaseIndex) -> f64 {
        for uptimes in self.buffs.iter() {
            if uptimes.id == buff_id {
                return uptimes
                    .buff_data
                    .get(phase_index.0)
                    .map(|bd| bd.uptime)
                    .unwrap_or_default();
            }
        }
        0.
    }
    pub fn is_enemy_player(&self) -> bool {
        self.enemy_player
    }
    pub fn get_breakbar_percents_at_time(&self, time_ms: i64) -> f64 {
        let mut current = 100.0;
        for element in &self.breakbar_percents {
            if (element[0] as i64) < time_ms {
                current = element[1]
            } else {
                break;
            }
        }
        current
    }

    pub fn get_final_buff_count(&self, buff_id: i64) -> i32 {
        self.buffs
            .iter()
            .find(|p| p.id == buff_id)
            .map(|buff| buff.get_final_count())
            .unwrap_or(0)
    }

    pub fn get_buff_count_at_time(&self, buff_id: i64, time: Duration) -> i32 {
        self.buffs
            .iter()
            .find(|p| p.id == buff_id)
            .map(|buff| buff.get_count_at_time(time))
            .unwrap_or(0)
    }
}
