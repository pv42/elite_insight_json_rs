use std::sync::Arc;

use chrono::Duration;
use serde::{Deserialize, Serialize};

use super::{
    ei_actor::{EIActor, EIActorStruct},
    ei_data_types::{
        EIBuffsUptime, EIConsumable, EIDamageDist, EIDamageModifierData, EIDamageModifierItem,
        EIDefenseAll, EIInstanceId, EIPlayerBuffsGeneration, EIPlayerSupport, EIRotation,
        EIWeaponType, EIDPS,
    },
    ei_ext_heal::EIEXTPlayerHealingStats,
    ei_stats::{EIGameplayStats, EIGameplayStatsAll},
    helper_types::{
        DamageSource, DamageType, PhaseIndex, Pos2D, TargetIndex, WeaponCategory, WeaponSet,
        WeaponSlot,
    },
};

// just use the impl of EIActor
impl EIActor for EIPlayer {
    /// Player character name
    fn name(&self) -> &str {
        self.ei_actor.name()
    }
    /// Total health, is not available (`None`) for players
    fn total_health(&self) -> Option<i32> {
        self.ei_actor.total_health()
    }
    fn condition_rank(&self) -> u32 {
        self.ei_actor.condition_rank()
    }
    fn concentration_rank(&self) -> u32 {
        self.ei_actor.concentration_rank()
    }
    fn healing_rank(&self) -> u32 {
        self.ei_actor.healing_rank()
    }
    fn toughness_rank(&self) -> u32 {
        self.ei_actor.toughness_rank()
    }
    fn hitbox_height(&self) -> u32 {
        self.ei_actor.hitbox_height()
    }
    fn hitbox_width(&self) -> u32 {
        self.ei_actor.hitbox_width()
    }
    fn instance_id(&self) -> EIInstanceId {
        self.ei_actor.instance_id()
    }
    fn is_fake(&self) -> bool {
        self.ei_actor.is_fake()
    }
    fn get_total_damage(&self, damage_type: DamageType, damage_source: DamageSource) -> i32 {
        self.ei_actor.get_total_damage(damage_type, damage_source)
    }
    fn get_damage_phase(
        &self,
        damage_type: DamageType,
        damage_source: DamageSource,
        phase_index: PhaseIndex,
    ) -> i32 {
        self.ei_actor
            .get_damage_phase(damage_type, damage_source, phase_index)
    }
    fn get_total_dps(&self, damage_type: DamageType, damage_source: DamageSource) -> i32 {
        self.ei_actor.get_total_dps(damage_type, damage_source)
    }
    fn get_dps_phase(
        &self,
        damage_type: DamageType,
        damage_source: DamageSource,
        phase_index: PhaseIndex,
    ) -> i32 {
        self.ei_actor
            .get_dps_phase(damage_type, damage_source, phase_index)
    }
    fn get_total_breakbar_damage(&self, damage_source: DamageSource) -> f64 {
        self.ei_actor.get_total_breakbar_damage(damage_source)
    }
    fn total_stats(&self) -> Option<&EIGameplayStatsAll> {
        self.ei_actor.total_stats()
    }
    fn stats_phase(&self, phase_index: PhaseIndex) -> Option<&EIGameplayStatsAll> {
        self.ei_actor.stats_phase(phase_index)
    }
    fn total_defense_stats(&self) -> Option<&EIDefenseAll> {
        self.ei_actor.total_defense_stats()
    }
    fn get_damage_at_time(&self, damage_type: DamageType, time: Duration) -> Option<i32> {
        self.ei_actor.get_damage_at_time(damage_type, time)
    }
    fn get_breakbar_damage_at_time(&self, time: Duration) -> Option<f64> {
        self.ei_actor.get_breakbar_damage_at_time(time)
    }
    fn get_skills_used(&self) -> Option<&Arc<[EIRotation]>> {
        self.ei_actor.get_skills_used()
    }
    fn get_condition_number_at_time(&self, time: Duration) -> i32 {
        self.ei_actor.get_condition_number_at_time(time)
    }
    fn get_boon_number_at_time(&self, time: Duration) -> Option<i32> {
        self.ei_actor.get_boon_number_at_time(time)
    }
    fn get_minion_number_at_time(&self, time: Duration) -> i32 {
        self.ei_actor.get_minion_number_at_time(time)
    }
    fn get_health_percent_at_time(&self, time: Duration) -> f64 {
        self.ei_actor.get_health_percent_at_time(time)
    }
    fn get_barrier_percent_at_time(&self, time: Duration) -> Option<f64> {
        self.ei_actor.get_barrier_percent_at_time(time)
    }

    /// Gets the position of the player a given time after the fight start
    ///
    /// Returns `None` if no combat replay data is present, it is empty or the provided time is negative
    /// returns last if time is past last combat replay entry
    /// otherwise interpolate between closesest entries
    fn get_position_at_time(&self, time: Duration, poll_rate: i32) -> Option<Pos2D> {
        self.ei_actor.get_position_at_time(time, poll_rate)
    }

    /// returns None if no combat replay data is present, it is empty or the provided time is negative
    /// returns last if time is past last combat replay entry
    /// otherwise interpolate between closesest entries
    fn get_orientation_at_time(&self, time: Duration, poll_rate: i32) -> Option<f32> {
        self.ei_actor.get_orientation_at_time(time, poll_rate)
    }
    /// returns None if no combat replay data is present
    fn is_dead_at_time(&self, time: Duration) -> Option<bool> {
        self.ei_actor.is_dead_at_time(time)
    }
    /// returns None if no combat replay data is present
    fn is_dc_at_time(&self, time: Duration) -> Option<bool> {
        self.ei_actor.is_dc_at_time(time)
    }
    /// returns None if no combat replay data is present
    fn is_down_at_time(&self, time: Duration) -> Option<bool> {
        self.ei_actor.is_down_at_time(time)
    }
}

#[derive(Debug, Deserialize, Serialize, Clone, Hash, PartialEq, Eq)]
pub enum Profession {
    // Base
    Guardian,
    Warrior,
    Engineer,
    Ranger,
    Thief,
    Elementalist,
    Mesmer,
    Necromancer,
    Revenant,
    // HoT
    Daredevil,
    Tempest,
    Berserker,
    Druid,
    Scrapper,
    Chronomancer,
    Herald,
    Reaper,
    Dragonhunter,
    // PoF
    Firebrand,
    Scourge,
    Mirage,
    Renegade,
    Soulbeast,
    Weaver,
    Holosmith,
    Spellbreaker,
    Deadeye,
    // EoD
    Mechanist,
    Virtuoso,
    Harbinger,
    Vindicator,
    Catalyst,
    Bladesworn,
    Willbender,
    Specter,
    Untamed,
    // other
    NPC,
    Gadget,
    Sword,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIPlayer {
    account: String,
    group: i32,
    has_commander_tag: bool,
    pub profession: Profession,
    #[serde(rename = "friendlyNPC")]
    pub friendly_npc: Option<bool>,
    pub not_in_squad: Option<bool>,
    #[serde(rename = "guildID")]
    pub guild_id: Option<String>,
    weapons: [EIWeaponType; 8],
    #[serde(default)] // TODO better default ???
    active_clones: Vec<[i32; 2]>,
    // todo slice
    dps_targets: Vec<Vec<EIDPS>>,
    #[serde(rename = "targetDamage1S")]
    // todo slice
    target_damage_1s: Vec<Vec<Vec<i32>>>,
    // todo slice
    #[serde(rename = "targetPowerDamage1S")]
    target_power_damage_1s: Option<Vec<Vec<Vec<i32>>>>,
    // todo slice
    #[serde(rename = "targetConditionDamage1S")]
    target_condition_damage_1s: Option<Vec<Vec<Vec<i32>>>>,
    // todo slice
    #[serde(rename = "targetBreakbarDamage1S")]
    target_breakbar_damage_1s: Option<Vec<Vec<Vec<f64>>>>,
    // todo slice
    target_damage_dist: Vec<Vec<Vec<EIDamageDist>>>,
    // todo slice
    stats_targets: Vec<Vec<EIGameplayStats>>,
    support: Arc<[EIPlayerSupport]>,
    damage_modifiers: Arc<[EIDamageModifierData]>,
    // todo slice
    damage_modifiers_target: Vec<Vec<EIDamageModifierData>>,
    buff_uptimes: Arc<[EIBuffsUptime]>,
    self_buffs: Option<Arc<[EIPlayerBuffsGeneration]>>,
    #[serde(default)]
    group_buffs: Vec<EIPlayerBuffsGeneration>,
    off_group_buffs: Option<Arc<[EIPlayerBuffsGeneration]>>,
    #[serde(default)]
    squad_buffs: Vec<EIPlayerBuffsGeneration>,
    buff_uptimes_active: Arc<[EIBuffsUptime]>,
    self_buffs_active: Option<Arc<[EIPlayerBuffsGeneration]>>,
    #[serde(default)]
    group_buffs_active: Vec<EIPlayerBuffsGeneration>,
    off_group_buffs_active: Option<Arc<[EIPlayerBuffsGeneration]>>,
    #[serde(default)]
    squad_buffs_active: Vec<EIPlayerBuffsGeneration>,
    #[serde(default)]
    consumables: Vec<EIConsumable>,
    active_times: Arc<[i64]>,
    #[serde(rename = "EXTHealingStats")]
    ext_healing_stats: Option<EIEXTPlayerHealingStats>,
    // TODO ext_barrier_stats: EIEXTPlayerBarrierStats,
    // everything after here is inhereted from Actor
    #[serde(flatten)]
    ei_actor: EIActorStruct,
}

impl EIPlayer {
    pub fn account(&self) -> &str {
        &self.account
    }
    pub fn sub_group(&self) -> i32 {
        self.group
    }
    pub fn has_commander_tag(&self) -> bool {
        self.has_commander_tag
    }
    pub fn get_dps_at_time(&self, time_ms: i64) -> i32 {
        self.get_dps_at_time_tp(time_ms, 0, 0)
    }
    pub fn get_dps_at_time_t(&self, time_ms: i64, target_index: usize) -> i32 {
        self.get_dps_at_time_tp(time_ms, target_index, 0)
    }
    pub fn get_dps_at_time_tp(&self, time_ms: i64, target_index: usize, phase_index: i32) -> i32 {
        let index = dbg!((time_ms / 1000) as usize);

        let dps_vec = self
            .target_damage_1s
            .get(target_index)
            .unwrap()
            .get(phase_index as usize)
            .unwrap();
        if index >= dps_vec.len() - 1 {
            return 0;
        }
        dbg!(dps_vec.get(index + 1).unwrap()) - dbg!(dps_vec.get(index).unwrap())
    }

    pub fn get_weapon_type(
        &self,
        land_or_water: WeaponCategory,
        set: WeaponSet,
        slot: WeaponSlot,
    ) -> &EIWeaponType {
        let internal_slot = land_or_water as usize * 4 + set as usize * 2 + slot as usize;
        if slot == WeaponSlot::OffHandSlot
            && self.weapons.get(internal_slot).unwrap() == &EIWeaponType::TwoHand
        {
            return self.weapons.get(internal_slot - 1).unwrap();
        }
        return self.weapons.get(internal_slot).unwrap();
    }

    pub fn get_all_weapons(&self) -> &[EIWeaponType; 8] {
        &self.weapons
    }

    pub fn count_weapon_type(&self, weapon: &EIWeaponType) -> i32 {
        let mut count = 0;
        for eq_weapon in &self.weapons {
            if eq_weapon == weapon {
                count += 1;
            }
        }
        count
    }

    // todo splitt this up
    pub fn get_target_dps(&self, target_index: usize) -> Option<&EIDPS> {
        self.dps_targets.get(target_index)?.first()
    }

    pub fn get_all_targets_cc(&self) -> f64 {
        self.dps_targets
            .iter()
            .enumerate()
            .map(|(index, _)| {
                self.get_breakbar_damage(
                    TargetIndex(index),
                    PhaseIndex::full_fight(),
                    DamageSource::All,
                )
            })
            .sum()
    }

    pub fn get_all_targets_dps(&self) -> i32 {
        self.dps_targets
            .iter()
            .enumerate()
            .map(|(index, _)| self.dps(TargetIndex(index)))
            .sum()
    }

    pub fn get_all_targets_damage(&self, damage_type: DamageType) -> i32 {
        self.dps_targets
            .iter()
            .enumerate()
            .map(|(index, _)| {
                self.get_damage(
                    TargetIndex(index),
                    PhaseIndex::full_fight(),
                    damage_type,
                    DamageSource::All,
                )
            })
            .sum()
    }

    pub fn get_buff_uptime_total(&self, buff_id: i64) -> f64 {
        self.get_buff_uptime_phase(buff_id, PhaseIndex::full_fight())
    }

    pub fn get_buff_uptime_phase(&self, buff_id: i64, phase_index: PhaseIndex) -> f64 {
        for uptimes in self.buff_uptimes.iter() {
            if uptimes.id == buff_id {
                return uptimes.buff_data.get(phase_index.0).unwrap().uptime;
            }
        }
        0.
    }

    pub fn get_group_buff_generation_total(&self, buff_id: i64) -> f64 {
        for generation in &self.group_buffs {
            if generation.id == buff_id {
                return generation.buff_data.first().unwrap().generation;
            }
        }
        0.0
    }

    pub fn get_off_group_buff_generation_total(&self, buff_id: i64) -> f64 {
        if let Some(off_group_buffs) = &self.off_group_buffs {
            for generation in off_group_buffs.iter() {
                if generation.id == buff_id {
                    return generation.buff_data.first().unwrap().generation;
                }
            }
        }
        0.0
    }

    pub fn get_squad_buff_generation_total(&self, buff_id: i64) -> f64 {
        for generation in &self.squad_buffs {
            if generation.id == buff_id {
                // todo unwrap
                return generation.buff_data.first().unwrap().generation;
            }
        }
        0.0
    }

    pub fn get_damage_mod_uptime(&self, damage_mod_id: i32) -> Option<&EIDamageModifierItem> {
        for damage_mod_data in self.damage_modifiers.iter() {
            if damage_mod_data.id == damage_mod_id {
                return damage_mod_data.damage_modifiers.first();
            }
        }
        None
    }

    /// Time passed on ressurecting in ceratain phase.
    /// Return 0 if phase is invalid
    pub fn resurrect_time(&self, phase_index: PhaseIndex) -> Duration {
        Duration::milliseconds(
            (self
                .support
                .get(phase_index.0)
                .map_or(0.0, |sup| sup.resurrect_time * 1000.0)) as i64,
        )
    }
    /// Total duration of condition cleansed on a squad mate in ceratain phase.
    /// Return 0 if phase is invalid
    pub fn condi_cleanse_time(&self, phase_index: PhaseIndex) -> Duration {
        Duration::milliseconds(
            (self
                .support
                .get(phase_index.0)
                .map_or(0.0, |sup| sup.condi_cleanse_time * 1000.0)) as i64,
        )
    }
    /// Total duration of condition cleansed on a squad mate in ceratain phase.
    /// Return 0 if phase is invalid
    pub fn self_condi_cleanse_time(&self, phase_index: PhaseIndex) -> Duration {
        Duration::milliseconds(
            (self
                .support
                .get(phase_index.0)
                .map_or(0.0, |sup| sup.condi_cleanse_time_self * 1000.0)) as i64,
        )
    }
    /// Total duration of boons stripped from self in ceratain phase.
    /// Return 0 if phase is invalid
    pub fn boon_strips_time(&self, phase_index: PhaseIndex) -> Duration {
        Duration::milliseconds(
            (self
                .support
                .get(phase_index.0)
                .map_or(0.0, |sup| sup.boon_strips_time * 1000.0)) as i64,
        )
    }
    /// Number of time ressurected someone in ceratain phase.
    /// Return 0 if phase is invalid
    pub fn resurrects(&self, phase_index: PhaseIndex) -> i64 {
        self.support
            .get(phase_index.0)
            .map_or(0, |sup| sup.resurrects)
    }
    /// Number of time a condition was cleansed on a squad mate in ceratain phase.
    /// Return 0 if phase is invalid
    pub fn condi_cleanse(&self, phase_index: PhaseIndex) -> i64 {
        self.support
            .get(phase_index.0)
            .map_or(0, |sup| sup.condi_cleanse)
    }
    /// Number of time a condition was cleansed from self in ceratain phase.
    /// Return 0 if phase is invalid
    pub fn condi_cleanse_self(&self, phase_index: PhaseIndex) -> i64 {
        self.support
            .get(phase_index.0)
            .map_or(0, |sup| sup.condi_cleanse_self)
    }
    /// Number of time a boon was stripped in ceratain phase.
    /// Return 0 if phase is invalid
    pub fn boon_strips(&self, phase_index: PhaseIndex) -> i64 {
        self.support
            .get(phase_index.0)
            .map_or(0, |sup| sup.boon_strips)
    }

    fn get_dps_stats(&self, target_index: TargetIndex, phase_index: PhaseIndex) -> Option<&EIDPS> {
        if let Some(target_dps) = self.dps_targets.get(target_index.0) {
            target_dps.get(phase_index.0)
        } else {
            None
        }
    }

    /// Total DPS to a specific target.
    /// Equivalent to `get_dps(target_index, PhaseIndex::full_fight(), DamageType::Total, DamageSource::All)`
    /// Returns 0 if the target index is invalid.
    pub fn dps(&self, target_index: TargetIndex) -> i32 {
        self.get_dps(
            target_index,
            PhaseIndex::full_fight(),
            DamageType::Total,
            DamageSource::All,
        )
    }
    /// Total damage of the specified damage type and source against a specified target and phase.
    /// Return 0 if target or phase index are invalid.
    pub fn get_damage(
        &self,
        target_index: TargetIndex,
        phase_index: PhaseIndex,
        damage_type: DamageType,
        damage_source: DamageSource,
    ) -> i32 {
        let stats = self.get_dps_stats(target_index, phase_index);
        if let Some(stats) = stats {
            stats.get_damage(damage_type, damage_source)
        } else {
            0
        }
    }
    /// Total DPS of the specified damage type and source against a specified target and phase.
    /// Return 0 if target or phase index are invalid.
    pub fn get_dps(
        &self,
        target_index: TargetIndex,
        phase_index: PhaseIndex,
        damage_type: DamageType,
        damage_source: DamageSource,
    ) -> i32 {
        let stats = self.get_dps_stats(target_index, phase_index);
        if let Some(stats) = stats {
            stats.get_dps(damage_type, damage_source)
        } else {
            0
        }
    }
    /// Total breakbar damage (CC) against a specified target and phase
    /// Return 0 if target or phase index are invalid
    pub fn get_breakbar_damage(
        &self,
        target_index: TargetIndex,
        phase_index: PhaseIndex,
        damage_source: DamageSource,
    ) -> f64 {
        let stats = self.get_dps_stats(target_index, phase_index);
        if let Some(stats) = stats {
            stats.get_breakbar_damage(damage_source)
        } else {
            0.0
        }
    }

    pub fn consumables(&self) -> &[EIConsumable] {
        &self.consumables
    }

    ///  Time during which the player was active (not dead and not dc)
    pub fn active_time(&self, phase_index: PhaseIndex) -> Option<Duration> {
        self.active_times
            .get(phase_index.0)
            .map(|time_ms| Duration::milliseconds(*time_ms))
    }
}
