use chrono::Duration;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

pub trait EIGameplayStatsTrait {
    /// Number of damage hit.
    fn total_hits(&self) -> i32;
    /// Total damage.  
    ///
    /// only available in new EI versions
    fn total_damage(&self) -> Option<i32>;
    /// Number of direct damage hit.
    fn direct_hits(&self) -> i32;
    /// Total direct damage.
    ///
    /// only in new EI versions
    fn direct_damage(&self) -> Option<i32>;
    /// Number of connected direct damage hit.
    fn connected_direct_hits(&self) -> i32;
    /// Total connected direct damage.
    ///
    /// only in new EI versions
    fn connected_direct_damage(&self) -> Option<i32>;
    /// Number of connected damage hit. 
    /// 
    /// only in new EI versions
    fn connected_hits(&self) -> Option<i32>;
    /// only in new EI versions
    fn connected_damage(&self) -> Option<i32>;
    /// Number of critable hit.
    fn critable_hits(&self) -> i32;
    /// Number of crit.
    fn crit_hits(&self) -> i32;
    /// critical hit rate
    fn crit_rate(&self) -> f64;
    /// Total critical damage.
    fn crit_damage(&self) -> i32;
    /// Number of hits while flanking.
    fn flanking_hits(&self) -> i32;
    /// Number of hits while target was moving.
    fn against_moving_hits(&self) -> Option<i32>;
    /// Number of glanced hits.
    fn glance_hits(&self) -> i32;
    /// Number of missed hits.
    fn missed_hits(&self) -> i32;
    /// Number of evaded hits.
    fn evaded_hits(&self) -> i32;
    /// Number of blocked hits.
    fn blocked_hits(&self) -> i32;
    /// Number of hits that interrupted a skill.
    fn interrupting_hits(&self) -> i32;
    /// Number of hits against invulnerable targets.
    fn invulned_hits(&self) -> i32;
    /// Number of times killed target.
    fn killing_hits(&self) -> i32;
    /// Number of times downed target.
    fn downing_hits(&self) -> i32;
    /// Relevant for WvW, defined as the sum of damage done from 90% to down that led to a death.     
    fn down_contribution_damage(&self) -> Option<i32>;
    /// Number of times a Power based damage skill hits.
    ///
    /// only available in new EI versions
    fn connected_power_hits(&self) -> Option<i32>;
    /// Number of times a Power based damage skill hits while source is above 90% hp.
    ///
    /// only available in new EI versions
    fn connected_power_hits_above_90(&self) -> Option<i32>;
    /// Number of times a Condition based damage skill hits.
    ///
    /// only available in new EI versions
    fn connected_condi_hits(&self) -> Option<i32>;
    /// Number of times a Condition based damage skill hits while source is above 90% hp.
    ///
    /// only available in new EI versions
    fn connected_condi_hits_above_90(&self) -> Option<i32>;
    /// Number of times a skill hits while target is downed is downed.
    ///
    /// only available in new EI versions
    fn hits_against_downed(&self) -> Option<i32>;
    /// Damage done against downed target.
    ///
    /// only available in new EI versions
    fn damage_against_downed(&self) -> Option<i32>;
}

// some values only in some versions
#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIGameplayStats {
    total_damage_count: i32,
    total_dmg: Option<i32>,
    direct_damage_count: i32,
    direct_dmg: Option<i32>,
    connected_direct_damage_count: i32,
    connected_direct_dmg: Option<i32>,
    connected_damage_count: Option<i32>,
    connected_dmg: Option<i32>,
    critable_direct_damage_count: i32,
    critical_rate: i32,
    critical_dmg: i32,
    flanking_rate: i32,
    against_moving_rate: Option<i32>,
    glance_rate: i32,
    missed: i32,
    evaded: i32,
    blocked: i32,
    interrupts: i32,
    invulned: i32,
    killed: i32,
    downed: i32,
    down_contribution: Option<i32>,
    connected_power_count: Option<i32>,
    #[serde(rename = "connectedPowerAbove90HPCount ")]
    connected_power_above_90: Option<i32>,
    #[serde(rename = "connectedConditionAbove90HPCount")]
    connected_condition_above_90: Option<i32>,
    connected_condition_count: Option<i32>,
    against_downed_count: Option<i32>,
    against_downed_damage: Option<i32>,
}

impl EIGameplayStatsTrait for EIGameplayStats {
    fn total_hits(&self) -> i32 {
        self.total_damage_count
    }
    fn total_damage(&self) -> Option<i32> {
        self.total_dmg
    }

    fn direct_hits(&self) -> i32 {
        self.direct_damage_count
    }

    fn direct_damage(&self) -> Option<i32> {
        self.direct_dmg
    }

    fn connected_direct_hits(&self) -> i32 {
        self.connected_direct_damage_count
    }

    fn connected_direct_damage(&self) -> Option<i32> {
        self.connected_direct_dmg
    }

    fn connected_hits(&self) -> Option<i32> {
        self.connected_damage_count
    }

    fn connected_damage(&self) -> Option<i32> {
        self.connected_dmg
    }

    fn critable_hits(&self) -> i32 {
        self.critable_direct_damage_count
    }

    fn crit_hits(&self) -> i32 {
        self.critical_rate
    }

    fn crit_rate(&self) -> f64 {
        self.critical_rate as f64 / self.critable_direct_damage_count as f64
    }

    fn crit_damage(&self) -> i32 {
        self.critical_dmg
    }

    fn flanking_hits(&self) -> i32 {
        self.flanking_rate
    }

    fn against_moving_hits(&self) -> Option<i32> {
        self.against_moving_rate
    }

    fn glance_hits(&self) -> i32 {
        self.glance_rate
    }

    fn missed_hits(&self) -> i32 {
        self.missed
    }

    fn evaded_hits(&self) -> i32 {
        self.evaded
    }

    fn blocked_hits(&self) -> i32 {
        self.blocked
    }

    fn interrupting_hits(&self) -> i32 {
        self.interrupts
    }

    fn invulned_hits(&self) -> i32 {
        self.invulned
    }

    fn killing_hits(&self) -> i32 {
        self.killed
    }

    fn downing_hits(&self) -> i32 {
        self.downed
    }

    fn down_contribution_damage(&self) -> Option<i32> {
        self.down_contribution
    }

    fn connected_power_hits(&self) -> Option<i32> {
        self.connected_power_count
    }

    fn connected_power_hits_above_90(&self) -> Option<i32> {
        self.connected_power_above_90
    }

    fn connected_condi_hits(&self) -> Option<i32> {
        self.connected_condition_count
    }

    fn connected_condi_hits_above_90(&self) -> Option<i32> {
        self.connected_condition_above_90
    }

    fn hits_against_downed(&self) -> Option<i32> {
        self.against_downed_count
    }

    fn damage_against_downed(&self) -> Option<i32> {
        self.against_downed_damage
    }
}

fn deserialize_f64_maybe_nan<'de, D>(deserializer: D) -> Result<f64, D::Error>
where
    D: Deserializer<'de>,
{
    // we define a local enum type inside of the function
    // because it is untagged, serde will deserialize as the first variant
    // that it can
    #[derive(Deserialize)]
    #[serde(untagged)]
    enum MaybeNA<U> {
        // if it can be parsed as Option<T>, it will be
        Value(U),
        // otherwise try parsing as a string
        NAString(String),
    }

    // deserialize into local enum
    let value: MaybeNA<f64> = Deserialize::deserialize(deserializer)?;
    match value {
        // if parsed as T or None, return that
        MaybeNA::Value(value) => Ok(value),

        // otherwise, if value is string an "n/a", return None
        // (and fail if it is any other string)
        MaybeNA::NAString(string) => {
            if string == "NaN" {
                Ok(f64::NAN)
            } else {
                Err(serde::de::Error::custom("Unexpected string"))
            }
        }
    }
}

fn serialize_f64_nan_to_str<S>(value: &f64, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    if value.is_nan() {
        serializer.serialize_str("NaN")
    } else {
        serializer.serialize_f64(*value)
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EIGameplayStatsAll {
    wasted: i32,
    time_wasted: f64,
    saved: i32,
    time_saved: f64,
    #[serde(
        deserialize_with = "deserialize_f64_maybe_nan",
        serialize_with = "serialize_f64_nan_to_str"
    )]
    stack_dist: f64,
    dist_to_com: f64,
    avg_boons: f64,
    avg_active_boons: f64,
    avg_conditions: f64,
    avg_active_conditions: f64,
    swap_count: i32,
    skill_cast_uptime: Option<f64>,
    #[serde(rename = "skillCastUptimeNoAA")]
    skill_cast_uptime_no_aa: Option<f64>,
    // inhereted from GameplayStats
    #[serde(flatten)]
    ei_stats: EIGameplayStats,
}

impl EIGameplayStatsAll {
    /// Number of time you interrupted your cast.
    pub fn skills_interrupted_count(&self) -> i32 {
        self.wasted
    }
    /// Time wasted by interrupting your cast.
    pub fn skills_interrupted_time(&self) -> Duration {
        Duration::milliseconds((self.time_saved * 1000.0) as i64)
    }
    /// Number of time you skipped an aftercast.
    pub fn skipped_aftercasts(&self) -> i32 {
        self.saved
    }
    /// Number of time you skipped an aftercast.
    pub fn saved_aftercasts_time(&self) -> Duration {
        Duration::milliseconds((self.time_saved * 1000.0) as i64)
    }
    /// Distance to the epicenter of the squad.
    pub fn avg_dist_to_stack(&self) -> f64 {
        self.stack_dist
    }
    /// Distance to the commander of the squad. Only when a player with commander tag is present.
    pub fn avg_dist_to_com(&self) -> f64 {
        self.dist_to_com
    }
    /// Average amount of boons.
    pub fn avg_boons(&self) -> f64 {
        self.avg_active_boons
    }
    /// Average amount of boons over active time.
    pub fn avg_boons_active_time(&self) -> f64 {
        self.avg_active_conditions
    }
    /// Average amount of conditions.
    pub fn avg_condis(&self) -> f64 {
        self.avg_conditions
    }
    /// Average amount of conditions over active time.
    pub fn avg_condis_active_time(&self) -> f64 {
        self.avg_active_conditions
    }
    /// Number of time a weapon swap happened.
    pub fn weapon_swap_count(&self) -> i32 {
        self.swap_count
    }
    /// % of time in combat spent in animation
    pub fn skill_cast_uptime(&self) -> Option<f64> {
        self.skill_cast_uptime
    }
    /// % of time in combat spent in animation, excluding auto attack skills
    pub fn skill_cast_uptime_no_aa(&self) -> Option<f64> {
        self.skill_cast_uptime_no_aa
    }
}

impl EIGameplayStatsTrait for EIGameplayStatsAll {
    fn total_hits(&self) -> i32 {
        self.ei_stats.total_hits()
    }

    fn total_damage(&self) -> Option<i32> {
        self.ei_stats.total_damage()
    }

    fn direct_hits(&self) -> i32 {
        self.ei_stats.direct_hits()
    }

    fn direct_damage(&self) -> Option<i32> {
        self.ei_stats.direct_damage()
    }

    fn connected_direct_hits(&self) -> i32 {
        self.ei_stats.connected_direct_hits()
    }

    fn connected_direct_damage(&self) -> Option<i32> {
        self.ei_stats.connected_direct_damage()
    }

    fn connected_hits(&self) -> Option<i32> {
        self.ei_stats.connected_hits()
    }

    fn connected_damage(&self) -> Option<i32> {
        self.ei_stats.connected_damage()
    }

    fn critable_hits(&self) -> i32 {
        self.ei_stats.critable_hits()
    }

    fn crit_hits(&self) -> i32 {
        self.ei_stats.crit_hits()
    }

    fn crit_rate(&self) -> f64 {
        self.ei_stats.crit_rate()
    }

    fn crit_damage(&self) -> i32 {
        self.ei_stats.crit_damage()
    }

    fn flanking_hits(&self) -> i32 {
        self.ei_stats.flanking_hits()
    }

    fn against_moving_hits(&self) -> Option<i32> {
        self.ei_stats.against_moving_hits()
    }

    fn glance_hits(&self) -> i32 {
        self.ei_stats.glance_hits()
    }

    fn missed_hits(&self) -> i32 {
        self.ei_stats.missed_hits()
    }

    fn evaded_hits(&self) -> i32 {
        self.ei_stats.evaded_hits()
    }

    fn blocked_hits(&self) -> i32 {
        self.ei_stats.blocked_hits()
    }

    fn interrupting_hits(&self) -> i32 {
        self.ei_stats.interrupting_hits()
    }

    fn invulned_hits(&self) -> i32 {
        self.ei_stats.invulned_hits()
    }

    fn killing_hits(&self) -> i32 {
        self.ei_stats.killing_hits()
    }

    fn downing_hits(&self) -> i32 {
        self.ei_stats.downing_hits()
    }

    fn down_contribution_damage(&self) -> Option<i32> {
        self.ei_stats.down_contribution_damage()
    }

    fn connected_power_hits(&self) -> Option<i32> {
        self.ei_stats.connected_power_hits()
    }

    fn connected_power_hits_above_90(&self) -> Option<i32> {
        self.ei_stats.connected_power_hits_above_90()
    }

    fn connected_condi_hits(&self) -> Option<i32> {
        self.ei_stats.connected_condi_hits()
    }

    fn connected_condi_hits_above_90(&self) -> Option<i32> {
        self.ei_stats.connected_condi_hits_above_90()
    }

    fn hits_against_downed(&self) -> Option<i32> {
        self.ei_stats.hits_against_downed()
    }

    fn damage_against_downed(&self) -> Option<i32> {
        self.ei_stats.damage_against_downed()
    }
}
