/// Index of a phase in a fight, phases are not disjunkt so any point in time is usually part of multiple phase.
/// There is usually a phase 0 that cointains the whole fight (accessable with `PhaseIndex::full_fight()`).
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct PhaseIndex(
    /// underlying index of the phase in the data structure if EliteInsight
    pub usize,
);

impl PhaseIndex {
    /// Phase index of the 'full fight' phase
    pub fn full_fight() -> Self {
        Self(0)
    }
}

/// Index of a tracked enemy targets in a fight. The main boss is usually index 0.
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct TargetIndex(pub usize);

/// Selector for a weapon set. Weapon sets can be toggled with weapon swap.
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum WeaponSet {
    WeaponSet1 = 0,
    WeaponSet2 = 1,
}

/// Weapon slot in a weapon set, 2-handed weapons are equiped in the main hand slot
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum WeaponSlot {
    MainHandSlot = 0,
    OffHandSlot = 1,
}

/// Determination if targeting the underwater weapon slots or the land ones.
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum WeaponCategory {
    LandWeapon = 0,
    UnderwaterWeapon = 1,
}

/// Position of a player or NPC with x and y coordinates in in game units (inch).
#[derive(Debug, PartialEq)]
pub struct Pos2D {
    pub x: f32,
    pub y: f32,
}

impl Pos2D {
    /// Euclidean  distance between two 2D Positions
    pub fn dist(&self, other: &Pos2D) -> f32 {
        f32::sqrt(f32::powi(self.x - other.x, 2) + f32::powi(self.y - other.y, 2))
    }
}

/// Type of damage
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum DamageType {
    /// All condition based damage including bleeding, burning, torment, poison and confusion
    Condi,
    /// All direct strike damage
    Power,
    /// All damage including power, condi and other (such as lifesteal) damage
    Total,
}

/// Whether to include minions, clones and pets in the damage
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum DamageSource {
    /// only damage done by the player directly
    ActorOnly,
    /// damage by the player and all thier entities
    All,
}

/// ID of GW2 buff, this includes boons, conditions, food effects, class specific buffs and other effects.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct BuffId(pub i64);
