use crate::log_manager::LMEncounter;

#[derive(Clone, PartialEq)]
pub struct GW2Encounter {
    pub id: LMEncounter,
    pub name_id: String,
    pub name: String,
    pub wing: i32,
    pub enemy_ids: Vec<i32>,
    pub short_name: String,
}

pub enum GW2SkillID {
    ProtectionBuff = 717,
    RegenerationBuff = 718,
    SwiftnessBuff = 719,
    FuryBuff = 725,
    VigorBuff = 726,
    MightBuff = 740,
    AegisBuff = 743,
    QuicknessBuff = 1187,
    BannerOfStrengthBuff = 14417,
    BannerOfDisciplineBuff = 14449,
    AlacrityBuff = 30328,
}

pub enum DamageModID {
    Scholar = -202180908,
}

pub fn get_encounter_by_name(name: &str) -> Option<GW2Encounter> {
    get_encounters()
        .into_iter()
        .find(|encounter| name == encounter.name)
}

pub fn get_encounter_by_id(boss_id: &str) -> Option<GW2Encounter> {
    get_encounters()
        .into_iter()
        .find(|encounter| boss_id == encounter.name_id)
}

pub fn get_encounter_by_lme(lme: LMEncounter) -> Option<GW2Encounter> {
    get_encounters()
    .into_iter()
    .find(|encounter| lme == encounter.id)
}

pub fn get_encounters() -> Vec<GW2Encounter> {
    vec![
        GW2Encounter {
            name_id: "vg".to_string(),
            name: "Vale Guardian".to_string(),
            wing: 1,
            enemy_ids: Vec::from([15438]),
            short_name: "VG".to_string(),
            id: LMEncounter::VG,
        },
        GW2Encounter {
            name_id: "gors".to_string(),
            name: "Gorseval the Multifarious".to_string(),
            wing: 1,
            enemy_ids: Vec::from([15429]),
            short_name: "Gorse".to_string(),
            id: LMEncounter::Gorseval,
        },
        GW2Encounter {
            name_id: "sab".to_string(),
            name: "Sabetha the Saboteur".to_string(),
            wing: 1,
            enemy_ids: Vec::from([15375]),
            short_name: "Sabetha".to_string(),
            id: LMEncounter::Sabetha,
        },
        GW2Encounter {
            name_id: "sloth".to_string(),
            name: "Slothasor".to_string(),
            wing: 2,
            enemy_ids: Vec::from([16123]),
            short_name: "Sloth".to_string(),
            id: LMEncounter::Slothasor,
        },
        GW2Encounter {
            name_id: "trio".to_string(),
            name: "Bandit Trio".to_string(),
            wing: 2,
            enemy_ids: Vec::from([]),
            short_name: "Trio".to_string(),
            id: LMEncounter::BanditTrio,
        }, // todo enemies
        GW2Encounter {
            name_id: "matt".to_string(),
            name: "Matthias Gabrel".to_string(),
            wing: 2,
            enemy_ids: Vec::from([16115]),
            short_name: "Matthias".to_string(),
            id: LMEncounter::Matthias,
        },
        GW2Encounter {
            name_id: "esc".to_string(),
            name: "Siege the Stronghold".to_string(),
            wing: 3,
            enemy_ids: Vec::from([]),
            short_name: "Escort".to_string(),
            id: LMEncounter::Escort,
        }, //# todo enemies
        GW2Encounter {
            name_id: "kc".to_string(),
            name: "Keep Construct".to_string(),
            wing: 3,
            enemy_ids: Vec::from([16235]),
            short_name: "KC".to_string(),
            id: LMEncounter::KC,
        },
        GW2Encounter {
            name_id: "tc".to_string(),
            name: "Twisted Castle".to_string(),
            wing: 3,
            enemy_ids: Vec::from([]),
            short_name: "TC".to_string(),
            id: LMEncounter::TC,
        }, //# todo enemies
        GW2Encounter {
            name_id: "xera".to_string(),
            name: "Xera".to_string(),
            wing: 3,
            enemy_ids: Vec::from([16246]),
            short_name: "Xera".to_string(),
            id: LMEncounter::Xera,
        },
        GW2Encounter {
            name_id: "cairn".to_string(),
            name: "Cairn the Indomitable".to_string(),
            wing: 4,
            enemy_ids: Vec::from([17194]),
            short_name: "Cairn".to_string(),
            id: LMEncounter::Cairn,
        },
        GW2Encounter {
            name_id: "mo".to_string(),
            name: "Mursaat Overseer".to_string(),
            wing: 4,
            enemy_ids: Vec::from([17172]),
            short_name: "MO".to_string(),
            id: LMEncounter::MO,
        },
        GW2Encounter {
            name_id: "sam".to_string(),
            name: "Samarog".to_string(),
            wing: 4,
            enemy_ids: Vec::from([17188]),
            short_name: "Samarog".to_string(),
            id: LMEncounter::Samarog,
        },
        GW2Encounter {
            name_id: "dei".to_string(),
            name: "Deimos".to_string(),
            wing: 4,
            enemy_ids: Vec::from([17154]),
            short_name: "Deimos".to_string(),
            id: LMEncounter::Deimos,
        },
        GW2Encounter {
            name_id: "sh".to_string(),
            name: "Soulless Horror".to_string(),
            wing: 5,
            enemy_ids: Vec::from([19767]),
            short_name: "SH".to_string(),
            id: LMEncounter::SH,
        },
        GW2Encounter {
            name_id: "rr".to_string(),
            name: "Desmina".to_string(),
            wing: 5,
            enemy_ids: Vec::from([]),
            short_name: "River".to_string(),
            id: LMEncounter::DesminaEscort,
        }, //# todo enemies
        GW2Encounter {
            name_id: "bk".to_string(),
            name: "Broken King".to_string(),
            wing: 5,
            enemy_ids: Vec::from([]),
            short_name: "BK".to_string(),
            id: LMEncounter::BrokenKing,
        }, //# todo enemies
        GW2Encounter {
            name_id: "dhuum".to_string(),
            name: "Dhuum".to_string(),
            wing: 5,
            enemy_ids: Vec::from([19450]),
            short_name: "Dhuum".to_string(),
            id: LMEncounter::Dhuum,
        },
        GW2Encounter {
            name_id: "ca".to_string(),
            name: "Conjured Amalgamate".to_string(),
            wing: 6,
            enemy_ids: Vec::from([43974]),
            short_name: "CA".to_string(),
            id: LMEncounter::ConjuredAmalgamate,
        },
        GW2Encounter {
            name_id: "twins".to_string(),
            name: "Twin Largos".to_string(),
            wing: 6,
            enemy_ids: Vec::from([21105, 21089]),
            short_name: "Twins".to_string(),
            id: LMEncounter::Twins,
        },
        GW2Encounter {
            name_id: "qadim".to_string(),
            name: "Qadim".to_string(),
            wing: 6,
            enemy_ids: Vec::from([20934]),
            short_name: "Qadim".to_string(),
            id: LMEncounter::Qadim,
        },
        GW2Encounter {
            name_id: "adina".to_string(),
            name: "Cardinal Adina".to_string(),
            wing: 7,
            enemy_ids: Vec::from([22006]),
            short_name: "Adina".to_string(),
            id: LMEncounter::Adina,
        },
        GW2Encounter {
            name_id: "sabir".to_string(),
            name: "Cardinal Sabir".to_string(),
            wing: 7,
            enemy_ids: Vec::from([21964]),
            short_name: "Sabir".to_string(),
            id: LMEncounter::Sabir,
        },
        GW2Encounter {
            name_id: "qpeer".to_string(),
            name: "Qadim the Peerless".to_string(),
            wing: 7,
            enemy_ids: Vec::from([22000]),
            short_name: "QTP".to_string(),
            id: LMEncounter::QTP,
        },
        GW2Encounter {
            name_id: "greer".to_string(),
            name: "Greer, the Blightbringer".to_string(),
            wing: 8,
            enemy_ids: Vec::from([]),
            short_name: "Greer".to_string(),
            id: LMEncounter::Greer,
        },
        GW2Encounter {
            name_id: "deci".to_string(),
            name: "Decima, the Stormsinger".to_string(),
            wing: 8,
            enemy_ids: Vec::from([]),
            short_name: "Decima".to_string(),
            id: LMEncounter::Decima,
        },
        GW2Encounter {
            name_id: "ura".to_string(),
            name: "Ura, the Steamshrieker".to_string(),
            wing: 8,
            enemy_ids: Vec::from([]),
            short_name: "Ura".to_string(),
            id: LMEncounter::Ura,
        },
        GW2Encounter {
            name_id: "ice".to_string(),
            name: "Icebrood Construct".to_string(),
            wing: 9,
            enemy_ids: Vec::from([22154]),
            short_name: "Ice".to_string(),
            id: LMEncounter::IcebroodConstruct,
        },
        GW2Encounter {
            name_id: "falln".to_string(),
            name: "Super Kodan Brothers".to_string(),
            wing: 9,
            enemy_ids: Vec::from([22343, 22481]),
            short_name: "Kodans".to_string(),
            id: LMEncounter::Kodans,
        },
        GW2Encounter {
            name_id: "frae".to_string(),
            name: "Fraenir of Jormag".to_string(),
            wing: 9,
            enemy_ids: Vec::from([22492]),
            short_name: "Fraenir".to_string(),
            id: LMEncounter::Fraenir,
        },
        GW2Encounter {
            name_id: "bone".to_string(),
            name: "Boneskinner".to_string(),
            wing: 9,
            enemy_ids: Vec::from([22521]),
            short_name: "Bone".to_string(),
            id: LMEncounter::Boneskinner,
        },
        GW2Encounter {
            name_id: "whisp".to_string(),
            name: "Whisper of Jormag".to_string(),
            wing: 9,
            enemy_ids: Vec::from([22711]),
            short_name: "Whisper".to_string(),
            id: LMEncounter::Whisper,
        },
        GW2Encounter {
            name_id: "trin".to_string(),
            name: "Aetherblade Hideout".to_string(),
            wing: 10,
            enemy_ids: Vec::from([24033]),
            short_name: "Mai Trin".to_string(),
            id: LMEncounter::MaiTrin,
        },
        GW2Encounter {
            name_id: "ankka".to_string(),
            name: "Xunlai Jade Junkyard".to_string(),
            wing: 10,
            enemy_ids: Vec::from([23957]),
            short_name: "Ankka".to_string(),
            id: LMEncounter::Ankka,
        },
        GW2Encounter {
            name_id: "li".to_string(),
            name: "Kaineng Overlook".to_string(),
            wing: 10,
            enemy_ids: Vec::from([24485, 24266]),
            short_name: "KO".to_string(),
            id: LMEncounter::MinisterLi,
        },
        GW2Encounter {
            name_id: "void".to_string(),
            name: "Harvest Temple".to_string(),
            wing: 10,
            enemy_ids: Vec::from([]),
            short_name: "HT".to_string(),
            id: LMEncounter::DragonVoid,
        }, //# todo enemies
        GW2Encounter {
            name_id: "olc".to_string(),
            name: "Old Lion's Court".to_string(),
            wing: 10,
            enemy_ids: Vec::from([25413, 25415, 25419, 25414, 25416, 25423]),
            short_name: "OLC".to_string(),
            id: LMEncounter::OLC,
        },
        GW2Encounter {
            name_id: "dagda".to_string(),
            name: "Cosmic Observatory".to_string(),
            wing: 11,
            enemy_ids: Vec::from([]),
            short_name: "Dagda".to_string(),
            id: LMEncounter::Dagda,
        },
        GW2Encounter {
            name_id: "cerus".to_string(),
            name: "Temple of Febe".to_string(),
            wing: 11,
            enemy_ids: Vec::from([]),
            short_name: "Cerus".to_string(),
            id: LMEncounter::Cerus,
        },
        GW2Encounter {
            name_id: "mama".to_string(),
            name: "MAMA".to_string(),
            wing: 12,
            enemy_ids: Vec::from([]),
            short_name: "MAMA".to_string(),
            id: LMEncounter::MAMA,
        },
        GW2Encounter {
            name_id: "siax".to_string(),
            name: "Siax the Corrupted".to_string(),
            wing: 12,
            enemy_ids: Vec::from([]),
            short_name: "Siax".to_string(),
            id: LMEncounter::Siax,
        },
        GW2Encounter {
            name_id: "enso".to_string(),
            name: "Ensolyss of the Endless Torment".to_string(),
            wing: 12,
            enemy_ids: Vec::from([]),
            short_name: "Ensolyss".to_string(),
            id: LMEncounter::Ensolyss,
        },
        GW2Encounter {
            name_id: "skor".to_string(),
            name: "Skorvald the Shattered".to_string(),
            wing: 13,
            enemy_ids: Vec::from([]),
            short_name: "Skorvald".to_string(),
            id: LMEncounter::Arkk,
        },
        GW2Encounter {
            name_id: "arriv".to_string(),
            name: "Artsariiv".to_string(),
            wing: 13,
            enemy_ids: Vec::from([]),
            short_name: "Artsariiv".to_string(),
            id: LMEncounter::Arkk,
        },
        GW2Encounter {
            name_id: "arkk".to_string(),
            name: "Arkk".to_string(),
            wing: 13,
            enemy_ids: Vec::from([]),
            short_name: "Arkk".to_string(),
            id: LMEncounter::Arkk,
        },
        GW2Encounter {
            name_id: "ai".to_string(),
            name: "Ai, Keeper of the Peak - Elemental".to_string(),
            wing: 14,
            enemy_ids: Vec::from([]),
            short_name: "Elemental Ai".to_string(),
            id: LMEncounter::AiEle,
        },
        GW2Encounter {
            name_id: "ai".to_string(),
            name: "Ai, Keeper of the Peak - Dark".to_string(),
            wing: 14,
            enemy_ids: Vec::from([]),
            short_name: "Dark Ai".to_string(),
            id: LMEncounter::AiDark,
        },
    ]
}
