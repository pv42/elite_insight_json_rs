/*!
 * This crate ...
 *
 * TODO
 *
 * # Usage
 *
 * TODO
 * # Example
 * TODO
 *
 * # Crate features
 * - dps_report_api: allows to upload logs to dps.report and download elite insight jsons from there, requires flate2, reqwest dependencies
 * - log_manager: allows loading data from the arc dps log managers database
 * - reqwest: adds functions that depend on blocking reqwest url calls as well as flate2 file IO operations, this may be renamed
 */

#[cfg(feature = "dps_report_api")]
pub mod dps_report_api;
pub mod ei_dto;
pub mod gw2_data;
#[cfg(feature = "log_manager")]
pub mod log_manager;

#[cfg(test)]
mod test {
    use std::str::FromStr;

    use chrono::{DateTime, Duration, FixedOffset};

    use crate::{
        dps_report_api::get_ei_data_cached,
        ei_dto::{
            ei_actor::EIActor,
            ei_data_types::{EIEncounterID, EIInstanceId, EIWeaponType},
            ei_player::Profession,
            helper_types::{
                DamageSource, DamageType, PhaseIndex, Pos2D, WeaponCategory, WeaponSet, WeaponSlot,
            },
        },
    };

    #[test]
    pub fn test() {
        let ei_log =
            get_ei_data_cached("https://dps.report/QAfp-20240614-223737_mama", None).unwrap();
        // log props
        assert_eq!(ei_log.elite_insights_version(), "2.69.2.0");
        assert_eq!(ei_log.trigger_id(), 17021);
        assert_eq!(ei_log.ei_encounter_id(), Some(EIEncounterID::MAMA));
        assert_eq!(ei_log.fight_name(), "MAMA");
        assert_eq!(ei_log.fight_icon(), "https://github.com/baaron4/GW2-Elite-Insights-Parser/blob/master/GW2EICustomAssets/FromGW2LogProofs/MAMA.png?raw=true");
        assert_eq!(ei_log.arc_version(), "EVTC20240612");
        assert_eq!(ei_log.gw2_build(), 164272);
        assert_eq!(ei_log.language(), "English");
        assert_eq!(ei_log.fractal_scale(), Some(96));
        assert_eq!(ei_log.language_id(), 0); // TODO type
        assert_eq!(ei_log.get_recording_player().name(), "Rayna Caslin");
        assert_eq!(
            ei_log.get_recording_player().account(),
            "PVzweiundvierzig.7381"
        );
        assert_eq!(
            ei_log.time_start(),
            DateTime::<FixedOffset>::from_str("2024-06-14T16:36:20-04:00").unwrap()
        );
        assert_eq!(
            ei_log.time_end(),
            DateTime::<FixedOffset>::from_str("2024-06-14T16:37:38-04:00").unwrap()
        );
        assert_eq!(ei_log.duration(), Duration::milliseconds(74608));
        assert_eq!(ei_log.log_start_offset(), Some(Duration::milliseconds(881)));
        assert_eq!(ei_log.success(), true);
        assert_eq!(ei_log.is_cm(), true);
        assert_eq!(ei_log.is_legendary_cm(), false);
        assert_eq!(ei_log.is_late_start(), false);
        assert_eq!(ei_log.missing_pre_event(), Some(false));
        assert_eq!(ei_log.anonymous(), false);
        assert_eq!(ei_log.is_detailed_wvw(), false);
        assert_eq!(ei_log.targets.len(), 4);
        let mama = ei_log.targets.first().unwrap();
        assert_eq!(mama.id(), 17021);
        assert_eq!(mama.final_health(), 0);
        assert_eq!(mama.final_barrier(), Some(0));
        assert_eq!(mama.barrier_percent(), Some(0.0));
        assert_eq!(mama.health_percent_burned(), 100.0);
        assert_eq!(mama.first_aware_ms(), -881); //TODO duration
        assert_eq!(mama.last_aware_ms(), 76660);
        // TODO buffs

        //
        assert_eq!(mama.is_enemy_player(), false);
        test_mama_actor(mama);

        let aegas = ei_log.players.first().unwrap();
        assert_eq!(aegas.sub_group(), 1);
        assert_eq!(aegas.account(), "Aegas.1069");
        assert_eq!(aegas.has_commander_tag(), false);
        assert_eq!(aegas.profession, Profession::Scrapper);
        assert_eq!(aegas.friendly_npc, Some(false));
        assert_eq!(aegas.not_in_squad, Some(false));
        assert_eq!(
            aegas.guild_id,
            Some("F5452694-2770-E711-80D3-E4115BD7B405".to_string())
        );
        assert_eq!(
            *aegas.get_weapon_type(
                WeaponCategory::LandWeapon,
                WeaponSet::WeaponSet2,
                WeaponSlot::MainHandSlot
            ),
            EIWeaponType::Hammer
        );
        assert_eq!(
            *aegas.get_weapon_type(
                WeaponCategory::LandWeapon,
                WeaponSet::WeaponSet2,
                WeaponSlot::OffHandSlot
            ),
            EIWeaponType::Hammer
        );
        assert_eq!(
            aegas
                .get_target_dps(0)
                .map(|d| d.get_dps(DamageType::Total, DamageSource::All)),
            Some(12587)
        );
        assert_eq!(
            aegas
                .get_target_dps(0)
                .map(|d| d.get_damage(DamageType::Total, DamageSource::All)),
            Some(939103)
        );
        assert_eq!(
            aegas
                .get_target_dps(0)
                .map(|d| d.get_dps(DamageType::Condi, DamageSource::All)),
            Some(301)
        );
        assert_eq!(
            aegas
                .get_target_dps(0)
                .map(|d| d.get_damage(DamageType::Condi, DamageSource::All)),
            Some(22460)
        );
        assert_eq!(
            aegas
                .get_target_dps(0)
                .map(|d| d.get_dps(DamageType::Power, DamageSource::All)),
            Some(12286)
        );
        assert_eq!(
            aegas
                .get_target_dps(0)
                .map(|d| d.get_damage(DamageType::Power, DamageSource::All)),
            Some(916643)
        );
        // dps Targets
        // [pwr|condi|all|cc] damage takes 1s
        assert_eq!(aegas.get_dps_at_time_tp(500, 0, 0), 10342);
        // TODO a lot of stats
        assert_eq!(aegas.resurrects(PhaseIndex::full_fight()), 0);
        assert_eq!(
            aegas.resurrect_time(PhaseIndex::full_fight()),
            Duration::seconds(0)
        );
        assert_eq!(aegas.condi_cleanse(PhaseIndex::full_fight()), 2);
        assert_eq!(
            aegas.condi_cleanse_time(PhaseIndex::full_fight()),
            Duration::milliseconds(149216)
        );
        assert_eq!(aegas.condi_cleanse_self(PhaseIndex::full_fight()), 4);
        assert_eq!(
            aegas.self_condi_cleanse_time(PhaseIndex::full_fight()),
            Duration::milliseconds(149898)
        );
        assert_eq!(aegas.boon_strips(PhaseIndex::full_fight()), 0);
        assert_eq!(
            aegas.boon_strips_time(PhaseIndex::full_fight()),
            Duration::seconds(0)
        );
        // more stats
        // TODO consumables
        assert_eq!(aegas.consumables().first().is_some(), true);
        // todo active time
        // todo ext healing stats
        assert_eq!(aegas.name(), "Artillery Barage");
        assert_eq!(aegas.total_health(), None);
        // bunch of duplicate things

        assert_eq!(
            ei_log
                .used_extensions
                .as_ref()
                .unwrap()
                .first()
                .unwrap()
                .name,
            "Healing Stats"
        );
        assert_eq!(
            ei_log
                .used_extensions
                .as_ref()
                .unwrap()
                .first()
                .unwrap()
                .version,
            "2.9rc1"
        );
        assert_eq!(
            ei_log
                .used_extensions
                .as_ref()
                .unwrap()
                .first()
                .unwrap()
                .revision,
            2
        );
        assert_eq!(
            ei_log
                .used_extensions
                .as_ref()
                .unwrap()
                .first()
                .unwrap()
                .signature,
            2627419289
        );
        assert_eq!(
            ei_log
                .used_extensions
                .as_ref()
                .unwrap()
                .first()
                .unwrap()
                .running_extension
                .len(),
            1
        );
        assert_eq!(
            ei_log
                .used_extensions
                .as_ref()
                .unwrap()
                .first()
                .unwrap()
                .running_extension
                .first()
                .unwrap(),
            "Rayna Caslin"
        );

        assert_eq!(
            ei_log
                .combat_replay_meta_data
                .as_ref()
                .unwrap()
                .inch_to_pixel,
            0.184
        );
        assert_eq!(
            ei_log
                .combat_replay_meta_data
                .as_ref()
                .unwrap()
                .polling_rate,
            150
        );
        assert_eq!(
            ei_log
                .combat_replay_meta_data
                .as_ref()
                .unwrap()
                .inch_to_pixel,
            0.184
        );
        assert_eq!(
            ei_log.combat_replay_meta_data.as_ref().unwrap().sizes,
            [750, 460]
        );
        assert_eq!(
            ei_log
                .combat_replay_meta_data
                .as_ref()
                .unwrap()
                .maps
                .first()
                .unwrap()
                .url,
            "https://i.imgur.com/zs03mc8.png"
        );
    }

    fn test_mama_actor(mama: &dyn EIActor) {
        // TODO bb
        assert_eq!(mama.name(), "MAMA");
        assert_eq!(mama.total_health(), Some(4973977));
        assert_eq!(mama.condition_rank(), 800);
        assert_eq!(mama.concentration_rank(), 0);
        assert_eq!(mama.healing_rank(), 800);
        assert_eq!(mama.toughness_rank(), 1374); // TODO rank/value
        assert_eq!(mama.hitbox_height(), 650);
        assert_eq!(mama.hitbox_width(), 290);
        assert_eq!(mama.instance_id(), EIInstanceId(189));
        //TODO assert_eq!(mama.team_id, 0)
        assert_eq!(mama.is_fake(), false);
        // DPS all
        // stats all
        // defense
        // tdmg dst
        // taken
        // rot
        // dmg 1s of all types
        // TODO crd icon assert_eq!(mama.)
        assert_eq!(
            mama.get_position_at_time(Duration::milliseconds(0), 150)
                .unwrap(),
            Pos2D {
                x: 430.222,
                y: 168.921
            }
        );
        assert_eq!(
            mama.get_orientation_at_time(Duration::milliseconds(150), 150)
                .unwrap(),
            -169.755
        );
    }
}
