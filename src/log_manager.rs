use chrono::{DateTime, Duration, FixedOffset};
use regex::Regex;
use std::{
    cmp::Eq,
    collections::{HashMap, HashSet},
    io,
    sync::Arc,
};
use thiserror::Error;

use num_derive::FromPrimitive;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use serde_repr::{Deserialize_repr, Serialize_repr};
use strum_macros::EnumIter;

#[derive(
    EnumIter,
    FromPrimitive,
    Serialize_repr,
    Deserialize_repr,
    PartialEq,
    Debug,
    Clone,
    Copy,
    Hash,
    Eq,
)]
#[repr(i32)]
pub enum LMEncounter {
    Other = 0,
    VG = 11,
    Gorseval = 13,
    Sabetha = 14,
    Slothasor = 21,
    BanditTrio = 22,
    Matthias = 23,
    Escort = 31,
    KC = 32,
    TC = 33,
    Xera = 34,
    Cairn = 41,
    MO = 42,
    Samarog = 43,
    Deimos = 44,
    SH = 51,
    DesminaEscort = 52,
    BrokenKing = 53,
    Eater = 54,
    Eyes = 55,
    Dhuum = 56,
    ConjuredAmalgamate = 61,
    Twins = 62,
    Qadim = 63,
    Adina = 71,
    Sabir = 72,
    QTP = 73,
    Greer = 81,
    Decima = 82,
    Ura = 83,
    MAMA = 10001,
    Siax = 10002,
    Ensolyss = 10003,
    Artsariiv = 10012,
    Skorvald = 10011,
    Arkk = 10013,
    AiEle = 10022,
    AiDark = 10023,
    AiBoth = 10024,
    Kanaxai = 10031,
    Eparch = 10041,
    Freezie = 20001,
    StandardGolem = 30001,
    MediumGolem = 30002,
    LargeGolem = 30003,
    IcebroodConstruct = 40001,
    Kodans = 40002,
    Fraenir = 40003,
    Boneskinner = 40004,
    Whisper = 40005,
    MaiTrin = 41001,
    Ankka = 41002,
    MinisterLi = 41003,
    DragonVoid = 41004,
    OLC = 42001,
    // todo verify next 2
    Dagda = 43001,
    Cerus = 43002,
    Mordremoth = 50001,
    WorldVersusWorld = 100000,
    Map = 200000,
}

impl LMEncounter {
    pub fn is_raid_boss(&self) -> bool {
        let int_val = *self as u32;
        10 < int_val && int_val < 100
    }
    pub fn is_fractal(&self) -> bool {
        let int_val = *self as u32;
        10000 < int_val && int_val < 11000
    }
    pub fn is_golem(&self) -> bool {
        let int_val = *self as u32;
        30001 < int_val && int_val < 30010
    }
    pub fn is_ibs_strike(&self) -> bool {
        let int_val = *self as u32;
        40000 < int_val && int_val < 40010
    }
    pub fn is_eod_strike(&self) -> bool {
        let int_val = *self as u32;
        41000 < int_val && int_val < 42010
    }
    pub fn is_soto_strike(&self) -> bool {
        let int_val = *self as u32;
        43000 < int_val && int_val < 43010
    }
    pub fn is_strike(&self) -> bool {
        let int_val = *self as u32;
        40000 < int_val && int_val < 44000
    }
}

// TODO not iter maybe
/// The result of an encounter, indicating whether the players failed or succeeded.
#[derive(
    EnumIter,
    FromPrimitive,
    Serialize_repr,
    Deserialize_repr,
    PartialEq,
    Debug,
    Clone,
    Copy,
    Hash,
    Eq,
)]
#[repr(i32)]
pub enum LMEncounterResult {
    Success = 0,
    Failure = 1,
    Unknown = 2,
}

///# An encounter mode or difficulty.
///
/// Challenge Modes are available for many Guild Wars 2 encounters, Emboldened is available in raids, and more
/// values may be introduced in the future if new modes are added to the game.
///
///
#[derive(
    EnumIter,
    FromPrimitive,
    Serialize_repr,
    Deserialize_repr,
    PartialEq,
    Debug,
    Clone,
    Copy,
    Hash,
    Eq,
)]
#[repr(i32)]
pub enum LMEncounterMode {
    Unknown = 0,
    /// The standard version of an encounter.
    Normal = 1,
    /// A harder version of an encounter.
    Challenge = 2,
    /// 1 stack of Emboldened (easy mode; extra stats).
    Emboldened1 = 3,
    /// 2 stacks of Emboldened (easy mode; extra stats).
    Emboldened2 = 4,
    /// 3 stacks of Emboldened (easy mode; extra stats).
    Emboldened3 = 5,
    /// 4 stacks of Emboldened (easy mode; extra stats).
    Emboldened4 = 6,
    /// 5 stacks of Emboldened (easy mode; extra stats).
    Emboldened5 = 7,
    /// LegendaryMode of Cerus
    LegendaryMode = 8,
}

/// Represents a language of the game client.
#[derive(
    EnumIter,
    FromPrimitive,
    Serialize_repr,
    Deserialize_repr,
    PartialEq,
    Debug,
    Clone,
    Copy,
    Hash,
    Eq,
)]
#[repr(i32)]
pub enum LMGameLanguage {
    English,
    French,
    German,
    Spanish,
    Chinese,
    Other,
}

/// Log manager parsing status of a log
#[derive(
    FromPrimitive, Serialize_repr, Deserialize_repr, PartialEq, Debug, Clone, Copy, Hash, Eq,
)]
#[repr(i32)]
pub enum LMParsingStatus {
    Unparsed,
    Parsing,
    Parsed,
    Failed,
}

///# Represents a profession of a player character in the game.
/// Each character may only have one profession.
#[derive(
    EnumIter,
    FromPrimitive,
    Serialize_repr,
    Deserialize_repr,
    PartialEq,
    Debug,
    Clone,
    Copy,
    Hash,
    Eq,
)]
#[repr(i32)]
/// Class of a player character, `LMProfession::None` is used if the class could not be determined.
///
/// # Example: iterate over all professions
/// ```
/// use elite_insight_json::log_manager::LMProfession;
/// use strum::IntoEnumIterator;
///
/// for prof in LMProfession::iter() {
///     if prof != LMProfession::None {
///         println!("{prof:?}");
///     }
/// }
/// ```
pub enum LMProfession {
    Guardian,
    Warrior,
    Engineer,
    Ranger,
    Thief,
    Elementalist,
    Mesmer,
    Necromancer,
    Revenant,
    None,
}

#[derive(
    EnumIter,
    FromPrimitive,
    Serialize_repr,
    Deserialize_repr,
    PartialEq,
    Debug,
    Clone,
    Copy,
    Hash,
    Eq,
)]
#[repr(i32)]
/// An elite specialization a player might have active or `None` if no elite spec is equipped or it could not be determined
pub enum LMEliteSpecialization {
    None,
    Dragonhunter,
    Berserker,
    Scrapper,
    Druid,
    Daredevil,
    Tempest,
    Chronomancer,
    Reaper,
    Herald,
    Soulbeast,
    Weaver,
    Holosmith,
    Deadeye,
    Mirage,
    Scourge,
    Spellbreaker,
    Firebrand,
    Renegade,
    Willbender,
    Bladesworn,
    Mechanist,
    Untamed,
    Specter,
    Catalyst,
    Virtuoso,
    Harbinger,
    Vindicator,
}

impl LMEliteSpecialization {
    /// returns the class of a elite specialization or `LMProfession::None` in case of `LMEliteSpecialization::None`
    pub fn get_profession(&self) -> LMProfession {
        match self {
            LMEliteSpecialization::None => LMProfession::None,
            LMEliteSpecialization::Dragonhunter
            | LMEliteSpecialization::Willbender
            | LMEliteSpecialization::Firebrand => LMProfession::Guardian,
            LMEliteSpecialization::Berserker
            | LMEliteSpecialization::Spellbreaker
            | LMEliteSpecialization::Bladesworn => LMProfession::Warrior,
            LMEliteSpecialization::Scrapper
            | LMEliteSpecialization::Holosmith
            | LMEliteSpecialization::Mechanist => LMProfession::Engineer,
            LMEliteSpecialization::Druid
            | LMEliteSpecialization::Soulbeast
            | LMEliteSpecialization::Untamed => LMProfession::Ranger,
            LMEliteSpecialization::Daredevil
            | LMEliteSpecialization::Deadeye
            | LMEliteSpecialization::Specter => LMProfession::Thief,
            LMEliteSpecialization::Tempest
            | LMEliteSpecialization::Weaver
            | LMEliteSpecialization::Catalyst => LMProfession::Elementalist,
            LMEliteSpecialization::Chronomancer
            | LMEliteSpecialization::Mirage
            | LMEliteSpecialization::Virtuoso => LMProfession::Mesmer,
            LMEliteSpecialization::Reaper
            | LMEliteSpecialization::Scourge
            | LMEliteSpecialization::Harbinger => LMProfession::Necromancer,
            LMEliteSpecialization::Herald
            | LMEliteSpecialization::Renegade
            | LMEliteSpecialization::Vindicator => LMProfession::Revenant,
        }
    }
}

#[derive(
    FromPrimitive, Serialize_repr, Deserialize_repr, PartialEq, Debug, Clone, Copy, Hash, Eq,
)]
#[repr(i32)]
/// Whether the player has a commander tag active at the start of the log
pub enum LMPlayerTag {
    // no commander tag
    None,
    /// commander tag
    Commander,
}

/// A loaded representation of the log manager cache file.
#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "PascalCase")]
pub struct LogManagerCache {
    /// Version of the cache file currently only 2 is supported.
    pub version: u32,
    /// Dictionary of log filenames with data extracted from a log file through processing and the state of this extraction.
    pub logs_by_filename: HashMap<String, LogManagerLog>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "PascalCase")]
/// A player in a log manager log entry
pub struct LMPlayer {
    /// Name of the character
    pub name: String,
    /// Account name of the player
    pub account_name: String,
    /// Subgroup index
    pub subgroup: i32,
    /// Profession of the character
    pub profession: LMProfession,
    /// Elite specialization of the character, `LMEliteSpecialization::None` if no elite spec
    pub elite_specialization: LMEliteSpecialization,
    /// Guild UID of the players represented guild if any
    pub guild_guid: Option<String>,
    /// Whether the player has a commander zag or not
    pub tag: LMPlayerTag,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "PascalCase")]
/// Data about the upload of the log to dps.report using the log manager
pub struct LMUploadData {
    /// URL of the last log upload if any succesful upload exists
    pub url: Option<String>,
    /// Error message if uploading failed
    pub processing_error: Option<String>,
    // TODO parse datetime string
    /// Time the log was uploaded
    pub upload_time: Option<String>,
}

/// Extra data for fractal encounters.
#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "PascalCase")]
pub struct LMFractalExtras {
    // TODO GW2 Skill ID
    /// Present mistlock instabilities, up to 3.
    pub mistlock_instabilities: Arc<[i32]>,
    /// Fractal difficulty scale from 1 to 100 or `None` if the data is absent from the log or was not determined by the log manager when it processed the log file
    pub fractal_scale: Option<i32>,
}

/// Extra data that is only relevant for some logs.
#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "PascalCase")]
pub struct LMLogExtras {
    /// Extra data for fractal encounters, `None` for other types of log
    pub fractal_extras: Option<LMFractalExtras>,
}

/// Information about the player generating this log file.
#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "PascalCase")]
pub struct LMPointOfView {
    /// The authors character name.
    pub character_name: String,
    /// The authors account name.
    pub account_name: String,
}

/// A C# exception that occurred during the log managers processing of a log.
///
/// An exception in the log processing could look like the following
/// ```
/// use elite_insight_json::log_manager::LMParsingException;
/// LMParsingException{
///     exception_name:"GW2Scratch.EVTCAnalytics.Exceptions.LogProcessingException".to_owned(),
///     message:"Multiple log end combat items found".to_owned(),
///     stack_trace:"   at GW2Scratch.EVTCAnalytics.Processing.LogProcessor.ProcessCombatItem(LogProcessorState state, ParsedCombatItem& item)\r\n   at GW2Scratch.EVTCAnalytics.Processing.LogProcessor.ProcessLog(SinglePassEVTCReader reader)\r\n   at GW2Scratch.EVTCAnalytics.Proce".to_owned(),
///     // TODO fill data
///     source: "".to_owned(),
///     inner_exception_data: None
/// };
/// ```
#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "PascalCase")]
pub struct LMParsingException {
    /// C# class of the exception caused.
    pub exception_name: String,
    /// Message of the exception
    pub message: String,
    /// The exceptions stack trace.
    pub stack_trace: String,
    /// Source of the exception.
    pub source: String,
    /// Optional inner exception that caused this exception to occur.
    pub inner_exception_data: Option<String>,
}

/// A tag (label) applied to a log file using the log manager
#[derive(Debug, Deserialize, Serialize, Clone, Hash, PartialEq, Eq)]
#[serde(rename_all = "PascalCase")]
pub struct LMTagInfo {
    /// Name of the Tag, freely determined by the user
    pub name: String,
}

/// Contains the data extracted from a log file through processing and the state of this extraction.
#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "PascalCase")]
pub struct LogManagerLog {
    /// The name of the corresponding log file.
    pub file_name: String,
    /// The players participating in the encounter recorded in this log.
    pub players: Option<Vec<LMPlayer>>,
    /// The result of the encounter recorded in this log.
    pub encounter_result: LMEncounterResult,
    /// The mode of the encounter recorded in this log.
    pub encounter_mode: LMEncounterMode,
    /// The Encounter that is recorded in this log.
    pub encounter: LMEncounter,
    /// The ID of the game map this log was recorded on.
    pub map_id: Option<i32>,
    /// The author of the log.
    pub point_of_view: Option<LMPointOfView>,
    /// The game version (build) used to generate this log.
    pub game_build: Option<i32>,
    /// The version of arcdps used to generate this log.
    pub evtc_version: Option<String>,
    /// The language of the game used to generate the log.
    pub game_language: LMGameLanguage,
    /// The name of the main target of the encounter.
    pub main_target_name: String,
    /// The health percentage of the target of the encounter. If there are multiple targets,
    /// the highest percentage is provided. If there are not targets `None` is provided.
    pub health_percentage: Option<f64>,
    /// Time when the encounter started.
    ///
    /// Is only an estimate if `IsEncounterStartTimePrecise` is false.
    /// If it's an estimate it will created by the log files info (C#: `System.IO.FileInfo`).
    /// If the creation of an estimate from the file data fails it will return the default of C#'s `DateTimeOffset`.
    pub encounter_start_time: DateTime<FixedOffset>,
    /* reduces performance so just use string
    #[serde(
        deserialize_with = "deserialize_duration",
        serialize_with = "serialize_duration"
    )]
    encounter_duration: Duration,
    */
    /// The duration of the encounter.
    encounter_duration: String,
    /// The upload status for uploads to dps.report, using Elite Insights on dps.report.
    #[serde(rename = "DpsReportEIUpload")]
    pub dps_report_ei_upload: LMUploadData,
    /// The current status of the data processing.
    pub parsing_status: LMParsingStatus,
    parse_time: DateTime<FixedOffset>,
    /// The amount of milliseconds the parsing of the log took or -1 if `ParsingStatus` is not `LMParsingStatus::Parsed`
    pub parse_milliseconds: i32,
    /// An exception if one was thrown during parsing. Will be null unless `ParsingStatus` is `LMParsingStatus::Failed`.
    pub parsing_exception: Option<LMParsingException>,
    pub parsing_version: String,
    /// Indicates whether the start time of the log is precise, or if it's an approximation based on the file creation date.
    pub is_encounter_start_time_precise: bool,
    /// The tags (and info about tags) applied to this log. Set of `LMTagInfo` rather than set of string for extensibility's sake.
    pub tags: HashSet<LMTagInfo>,
    /// Indicates whether a log is a favorite.
    pub is_favorite: bool,
    /// Extra data that is only relevant for some logs.
    pub log_extras: Option<LMLogExtras>,
    /// Indicates whether a log is missing the encounter start time
    pub missing_encounter_start: bool,
}

fn deserialize_duration<'de, D>(d: D) -> Result<Duration, D::Error>
where
    D: Deserializer<'de>,
{
    let string: &str = Deserialize::deserialize(d)?;
    deserialize_duration_value(string).ok_or(serde::de::Error::custom("Failed to parse duration."))
}

fn deserialize_duration_value(string: &str) -> Option<Duration> {
    let regex = Regex::new(r"^(-?)(\d{2}):(\d{2}):(\d{2})(.(\d+))?$").ok()?;
    let caps = regex.captures(string)?;
    let is_neg = caps.get(1).map(|m| m.as_str()).unwrap_or("") == "-";
    let hrs_str = caps.get(2)?.as_str();
    let mins_str = caps.get(3)?.as_str();
    let secs_str = caps.get(4)?.as_str();
    let frac_str = caps.get(6).map(|m| m.as_str()).unwrap_or("0000000");
    let hrs = hrs_str.parse::<i64>().ok()?;
    let mins = mins_str.parse::<i64>().ok()?;
    let secs = secs_str.parse::<i64>().ok()?;
    let f_len = frac_str.len() as u32;
    let frac = frac_str.parse::<i64>().ok()?;
    Some(Duration::nanoseconds(
        ((hrs * 60 + mins) * 60 + secs) * 1_000_000_000
            + frac * i64::pow(10, 9 - f_len) * (if is_neg { -1 } else { 1 }),
    ))
}

fn serialize_duration<S>(value: &Duration, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    let (value_pos, minus) = if value < &Duration::seconds(0) {
        (*value, "")
    } else {
        (Duration::seconds(0) - *value, "")
    };
    let str = format!(
        "{minus}{:02}:{:02}:{:02}.{:06}0",
        value_pos.num_hours(),
        value_pos.num_minutes() % 60,
        value_pos.num_seconds() % 60,
        value_pos.num_microseconds().unwrap_or(0) % 1_000_000
    );
    serializer.serialize_str(&str)
}

impl LogManagerLog {
    /// The duration of the encounter.
    ///
    /// Returns `None` if the value was not well formatted
    pub fn get_duration(&self) -> Option<Duration> {
        deserialize_duration_value(&self.encounter_duration)
    }
    /// Returns the time when the encounter was parsed by the log manager or `None` if it was not parsed yet.
    pub fn get_parse_time(&self) -> Option<DateTime<FixedOffset>> {
        if self.parsing_status == LMParsingStatus::Parsed {
            Some(self.parse_time)
        } else {
            None
        }
    }
}

#[derive(Error, Debug)]
/// An error that can occur when loading the ArcDPS LogManager Cache file
pub enum LogManagerCacheError {
    #[error("failed to read enviroment variable")]
    EnvVarError(#[from] std::env::VarError),
    #[error("failed to deserialize log manager cache")]
    DeserializeError(#[from] serde_json::Error),
    #[error("Failed to read from log manager cache file, does it exist ?")]
    FileReadError(#[from] io::Error),
}

impl LogManagerCache {
    /// Attempts to load the Arcdps Log Manager cache file from its default location.
    /// The path where the file is expected depends on the OS used.
    /// Only Linux and Windows are tested. To find the location of the local app data path the environment variables `%USERPROFILE%` (Win) or `$HOME` (other) must be present.
    ///
    /// # Return
    /// - the deserialized LogManagerCache struct loaded from the cache file
    /// - an error if the the required env variable could not be read, the log file could not be read or the file could not be deserialized.
    pub fn get_system_cache() -> Result<LogManagerCache, LogManagerCacheError> {
        let filepath = if cfg!(target_os = "windows") {
            let user_profile = std::env::var("USERPROFILE")?;
            user_profile + "/AppData/Local/ArcdpsLogManager/LogDataCache.json"
        } else {
            let user_profile = std::env::var("HOME")?;
            user_profile + "/.local/share/ArcdpsLogManager/LogDataCache.json"
        };
        Self::load_from_file(filepath)
    }

    /// Attempts to load the specified file as a Arcdps Log Manager cache file.
    /// # Return
    /// - the deserialized LogManagerCache struct loaded from the cache file
    /// - an error if the log file could not be read or the file could not be deserialized.
    pub fn load_from_file(filepath: String) -> Result<LogManagerCache, LogManagerCacheError> {
        let mut data = std::fs::read_to_string(filepath).unwrap();
        // strip a weird unicode char
        if data.chars().next().unwrap_or('{') != '{' {
            data = data[3..].to_string()
        }
        Ok(serde_json::from_str(&data)?)
    }
}
